//
//  Block.swift
//  SilenceApp
//
//  Created by Tayyab Mubeen on 25/05/2022.
//

import Foundation

struct Block: Codable {
    var blockedFrom: String?
    var blockedTo: String?
}
