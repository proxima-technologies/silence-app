//
//  Inbox.swift
//  SilenceApp
//
//  Created by Rana Asad on 18/05/2022.
//

import Foundation
struct Inbox: Codable {
    var id: String
    var inboxType: InboxType
    var isDeleted: Bool
    var isRead: Bool
    var joinedList: Array<String>
    var message: String
    var messageType: MessageType
    var receiverId: String
    var receiverImage: String
    var receiverName: String
    var senderId: String
    var senderName: String
    var senderImage: String
    var time: Int64
}
enum InboxType: String,Codable {
    case userInbox = "UserChat"
    case groupChat = "GroupChat"
}
enum MessageType: String,Codable {
    case textMessage = "Text"
    case englishSign = "SignEnImage"
    case ArabicSign = "SignArImage"
    case HebrewSign = "SignIwImage"
    case Image = "Image"
}
