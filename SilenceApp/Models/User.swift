//
//  User.swift
//  SilenceApp
//
//  Created by Rana Asad on 17/05/2022.
//

import Foundation
struct User: Codable {
    var activeStatus: Bool
    var contactList: Array<String>?
    var dateCreated: Int64
    var dateUpdated: Int64
    var device: Device
    var email: String
    var id: String
    var isDataSet: Bool
    var isNotificationEnabled: Bool
    var name: String
    var phoneNo: String
    var signInMethod: String
    var status: String
    var imageUrl: String
}
struct Device: Codable {
    var deviceType: String
    var manufacturer: String
    var model: String
    var sdk: Int
    var token: String
}
