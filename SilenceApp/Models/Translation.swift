//
//  Translation.swift
//  SilenceApp
//
//  Created by Rana Asad on 25/05/2022.
//

import Foundation
struct Translation: Codable {
    var translated_text: TranslatedText
}
