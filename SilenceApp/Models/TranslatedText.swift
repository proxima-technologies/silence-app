//
//  TranslatedText.swift
//  SilenceApp
//
//  Created by Rana Asad on 25/05/2022.
//

import Foundation
struct TranslatedText: Codable {
    var ar: String?
    var en: String?
    var iw: String?
}
