//
//  SignMessage.swift
//  SilenceApp
//
//  Created by Rana Asad on 23/05/2022.
//

import Foundation
struct SignMessage: Codable {
    var iconName: String?
    var type: String
    var value: String?
}
