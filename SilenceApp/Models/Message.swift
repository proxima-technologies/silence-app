//
//  Message.swift
//  SilenceApp
//
//  Created by Rana Asad on 23/05/2022.
//

import Foundation
struct Message: Codable {
    var duration: Int64?
    var inboxId: String
    var list: Array<SignMessage>?
    var message: String?
    var messageId: String
    var messageType: MessageType
    var seen: Dictionary<String,String>
    var senderId: String
    var senderName: String
    var sent: Bool
    var time: Int64
    var translationMap: Dictionary<String,String>
    var date: Date?
}
