//
//  UserDetailViewModel.swift
//  SilenceApp
//
//  Created by Tayyab Mubeen on 25/05/2022.
//

import UIKit


class UserDetailViewModel  {
    
    
    func getBlockList() {
        FirebaseSerivce.getShared().getAllBlockList { blockList, error in
            if error == nil, let blockList = blockList {
                StaticData.blockListArr = blockList
                print(blockList)
            }
        }
    }
    
    
    func blockUser(user:String) {
        let body1 = Block(blockedFrom: StaticData.user!.id, blockedTo: user)
        let body2 = Block(blockedFrom: user, blockedTo: StaticData.user!.id)
        FirebaseSerivce.getShared().blockUser(body1:body1,body2:body2,userId: user)
        StaticData.blockListArr.append(Block(blockedFrom: StaticData.user!.id, blockedTo: user))
    }
    
    func unBlockUser(user:String) {
        FirebaseSerivce.getShared().unBlockUser(userId: user)
        for (index, userId) in StaticData.blockListArr.enumerated() {
            if userId.blockedTo == user {
                StaticData.blockListArr.remove(at: index)
            }
        }
    }
}
