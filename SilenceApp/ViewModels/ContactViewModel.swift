//
//  ContactViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 19/05/2022.
//

import Foundation
import ContactsUI
import FirebaseAuth
class ContactViewModel {
    public var contactsArray: Array<User> = []
    public var tempContacts: Array<User> = []
    public var contactsResponse: (()->())?
    public var channelIdResponse: ((String)->())?
    init() {
        getAllUsers()
    }
    func getAllUsers() {
        FirebaseSerivce.getShared().getAllUsers(completion: {[weak self](users,error) in
            if let err = error {
                
            } else {
                guard let users = users else {
                    return
                }
                guard let contacts = self?.getContacts() else {
                    return
                }
                self?.filterUsers(contacts: contacts, users: users)
            }
            
        })
    }
    func filterUsers(contacts: Array<String>,users: Array<User>) {
        tempContacts.removeAll()
        for user in users {
            for contact in contacts {
                let contact1 = contact.replacingOccurrences(of: "(", with: "")
                let contact2 = contact1.replacingOccurrences(of: ")", with: "")
                let contact3 = contact2.replacingOccurrences(of: " ", with: "")
                let contact4 = contact3.replacingOccurrences(of: "-", with: "")
                if user.phoneNo.contains(contact4) {
                    contactsArray.append(user)
                }
            }
        }
        tempContacts.append(contentsOf: contactsArray)
        contactsResponse?()
    }
    func getContacts() -> [String]{
        let contactStore = CNContactStore()
        var contacts: Array<String> = []
        let key = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactImageDataKey,CNContactThumbnailImageDataKey,CNContactPhoneNumbersKey,CNContactEmailAddressesKey] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        try? contactStore.enumerateContacts(with: request, usingBlock: { (contact, stoppingPointer) in
            let phoneNumber: [String] = contact.phoneNumbers.map{ $0.value.stringValue }
            if phoneNumber.count > 0 {
                contacts.append(phoneNumber[0])
            }
            
        })
        return contacts
    }
    public func configureCell(tableView: UITableView,indexPath: IndexPath) -> MyContactTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyContactTableViewCell" , for: indexPath) as! MyContactTableViewCell
        let user = contactsArray[indexPath.row]
        let contactCellViewModel = ContactCellViewModel(user: user)
        cell.configure(contactCellViewModel: contactCellViewModel)
        return cell
    }
    public func findDocument(receiverUser: User) {
        guard let mineId = Auth.auth().currentUser?.uid else {
            return
        }
        let straightId = mineId + receiverUser.id
        let reverseId = receiverUser.id + mineId
        FirebaseSerivce.getShared().documentExist(id: straightId, completion: {[weak self] success in
            if success {
                self?.channelIdResponse?(straightId)
            } else {
                FirebaseSerivce.getShared().documentExist(id: reverseId, completion: { success in
                    if success {
                        self?.channelIdResponse?(reverseId)
                    } else {
                        self?.channelIdResponse?(straightId)
                    }
                })
            }
        })
    }
}
extension String {
    func removingLeadingSpaces() -> String {
        guard let index = firstIndex(where: { !CharacterSet(charactersIn: String($0)).isSubset(of: .whitespaces) }) else {
            return self
        }
        return String(self[index...])
    }
}
