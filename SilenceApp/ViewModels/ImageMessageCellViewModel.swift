//
//  ImageMessageCellViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 25/05/2022.
//

import Foundation
import FirebaseAuth
class ImageMessageCellViewModel {
    public var isMine = false
    public var message: Message!
    public var cellType: DateEnum!
    init(message: Message,cellType: DateEnum) {
        self.message = message
        self.cellType = cellType
        checkUser()
    }
    func checkUser() {
        guard let id = Auth.auth().currentUser?.uid else {
            return
        }
        if message.senderId == id {
            isMine = true
        } else {
            isMine = false
        }
    }
}
