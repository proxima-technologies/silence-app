//
//  SignUpViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 17/05/2022.
//

import Foundation
import UIKit
import FirebaseAuth
class SignUpViewModel {
    public var imageUploadResponse: ((String?)->())?
    public var signUpResponse: ((String?)->())?
    func uploadImage(image: UIImage) {
        FirebaseSerivce.getShared().uploadProfileImage(imagePath: "Profile", image: image, completion: {url in
            self.imageUploadResponse?(url)
        })
    }
    public func signUp(email: String,fullName: String,url: String,about: String,phoneNo: String) {
        let currentMills = Utils.getShared().getMillis()
        let deviceType = UIDevice().type
        let systemVersion = 15
        let device = Device(deviceType: "iOS", manufacturer: "Apple", model: deviceType.rawValue, sdk: systemVersion, token: "")
        guard let id = Auth.auth().currentUser?.uid else {
            return
        }
        let user = User(activeStatus: true, contactList: [], dateCreated: currentMills, dateUpdated: currentMills, device: device, email: email, id: id, isDataSet: true, isNotificationEnabled: false, name: fullName, phoneNo: phoneNo, signInMethod: "Phone", status: about,imageUrl: url)
        FirebaseSerivce.getShared().signUpUser(user: user, completion: {error in
            self.signUpResponse?(error)
        })
    }
    deinit {
        print("good")
    }
}
