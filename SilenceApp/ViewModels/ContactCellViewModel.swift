//
//  ContactCellViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 19/05/2022.
//

import Foundation
class ContactCellViewModel {
    private var user: User!
    init(user: User){
        self.user = user
    }
    func getUser() -> User {
        return user
    }
}
