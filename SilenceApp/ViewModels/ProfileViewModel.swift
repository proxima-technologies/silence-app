//
//  ProfileViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 19/05/2022.
//

import Foundation
import UIKit
class ProfileViewModel {
    public var imageUploadResponse: ((String?)->())?
    public var profileEdit: ProfileEdit = .Unknown
    func uploadImage(image: UIImage) {
        FirebaseSerivce.getShared().uploadProfileImage(imagePath: "Profile", image: image, completion: {url in
            self.imageUploadResponse?(url)
        })
    }
    func updateUser(user: User) {
        FirebaseSerivce.getShared().signUpUser(user: user, completion: {error in
        })
    }
    
}
enum ProfileEdit {
    case Name
    case Email
    case Status
    case Unknown
}
