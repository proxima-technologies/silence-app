//
//  HomeTabbarViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 25/05/2022.
//

import Foundation
import UIKit
class HomeTabbarViewModel {
    public var imageUploadResponse: ((String?)->())?
    func uploadImage(image: UIImage) {
        FirebaseSerivce.getShared().uploadProfileImage(imagePath: "Profile", image: image, completion: {url in
            self.imageUploadResponse?(url)
        })
    }
}
