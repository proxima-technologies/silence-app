//
//  OTPViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import Foundation
class OTPViewModel {
    public var verificationId: String = ""
    public var response: ((String?,String?)->())?
    public var userResponse: ((User?,String?)->())?
    
    public func authenticateOTP(otp: String) {
        FirebaseSerivce.getShared().authenticateOTP(otp: otp, verificationId: verificationId, completion: {id,error in
            self.response?(id,error)
        })
    }
    public func getUserDetail(id: String) {
        FirebaseSerivce.getShared().getUserDetails(id: id, completion: { user,error in
            self.userResponse?(user,error)
        })
    }
    public func resendCode(phoneNumber: String) {
        FirebaseSerivce.getShared().phoneAuthentication(phoneNumber: phoneNumber , completion: { verificationId,error in
            guard let id = verificationId else {
                return
            }
            self.verificationId = id
        })
    }
    deinit {
        print("good")
    }
}
