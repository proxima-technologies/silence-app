//
//  SplashViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 19/05/2022.
//

import Foundation
class SplashViewModel {
    public var userResponse: ((User?,String?)->())?
    func getUserDetails(userId: String) {
        FirebaseSerivce.getShared().getUserDetails(id: userId, completion: { user,error in
            self.userResponse?(user,error)
        })
    }
    deinit {
        print("good")
    }
}
