//
//  InboxViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 18/05/2022.
//

import Foundation
import FirebaseAuth
class MessageViewModel {
    public var messages: Array<Inbox> = []
    public var tempMessages: Array<Inbox> = []
    public var reloadTableView: (()->())?
    public var channelIdResponse: ((String)->())?
    public var currentUser: User?
    public func configureCell(tableView: UITableView,indexPath: IndexPath) -> MessageTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell" , for: indexPath) as! MessageTableViewCell
        let currentInbox = messages[indexPath.row]
        let messageCellViewModel = MessageCellViewModel(inbox: currentInbox)
        cell.configure(messageCellViewModel: messageCellViewModel)
        return cell
    }
    public func getMessages() {
        messages.removeAll()
        tempMessages.removeAll()
        guard let userId = Auth.auth().currentUser?.uid else {
            return
        }
        FirebaseSerivce.getShared().getInbox(id: userId, completion: { inbox,error,type in
            if let err = error {
                
            } else {
                guard let inbox = inbox else {
                    return
                }
                self.messages.removeAll(where: { $0.id == inbox.id})
                self.tempMessages.removeAll(where: { $0.id == inbox.id})
                self.messages.insert(inbox, at: 0)
                self.messages.sort(by: {$0.time > $1.time})
                self.tempMessages.insert(inbox, at: 0)
                self.tempMessages.sort(by: {$0.time > $1.time})
                self.reloadTableView?()
            }
        })
    }
    public func getUserDetail(id: String) {
        FirebaseSerivce.getShared().getUserDetails(id: id, completion: {[weak self] (user,error) in
            if error == nil {
                guard let user = user else {
                    return
                }
                self?.currentUser = user
                self?.findDocument(receiverUser: user)
            }
        })
    }
    public func findDocument(receiverUser: User) {
        guard let mineId = Auth.auth().currentUser?.uid else {
            return
        }
        let straightId = mineId + receiverUser.id
        let reverseId = receiverUser.id + mineId
        FirebaseSerivce.getShared().documentExist(id: straightId, completion: {[weak self] success in
            if success {
                self?.channelIdResponse?(straightId)
            } else {
                FirebaseSerivce.getShared().documentExist(id: reverseId, completion: { success in
                    if success {
                        self?.channelIdResponse?(reverseId)
                    } else {
                        self?.channelIdResponse?(straightId)
                    }
                })
            }
        })
    }
    func getUserId(inbox: Inbox) -> String {
        guard let userId = Auth.auth().currentUser?.uid else {
            return ""
        }
        if inbox.receiverId != userId {
            return inbox.receiverId
        } else {
            return inbox.senderId
        }
    }
    
}
