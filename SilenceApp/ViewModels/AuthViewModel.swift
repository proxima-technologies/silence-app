//
//  AuthViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import Foundation
class AuthViewModel {
    public var codeSent:((String?,String?)->())?
    
    public func getCode(countryCode: String, phoneNumber: String) {
        FirebaseSerivce.getShared().phoneAuthentication(phoneNumber: countryCode + phoneNumber , completion: { verificationId,error in
            guard let id = verificationId else {
                self.codeSent?(nil,error)
                return
            }
            self.codeSent?(id,nil)
        })
    }
    deinit {
        print("good")
    }
}
