//
//  ConversationViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 23/05/2022.
//
// Hello great
import Foundation
import UIKit
import FirebaseAuth
class ConversationViewModel {
    public var channelId: String = ""
    public var signMessages: Array<SignMessage> = []
    public var inboxResponse: ((Inbox?)->())?
    public var messageResponse: ((Int?)->())?
    public var keyboardType: KeyboardType = .Text
    public var tempKeyboardType: KeyboardType = .Text
    public var messages: Array<Message> = []
    private var dummyMessageList: Array<Message> = []
    private var isFirstLong = true
    public var imageUploadResponse: ((String?)->())?
    init() {
        let keyboard = ConfigManager.getInstance().getLanguage()
        if keyboard == "ar" {
            keyboardType = .Arabic
        } else if keyboard == "en" {
            keyboardType = .English
        } else if keyboard == "iw" {
            keyboardType = .Hebrew
        } else {
            keyboardType = .Text
        }
        tempKeyboardType = keyboardType
    }
    func uploadImage(image: UIImage) {
        FirebaseSerivce.getShared().uploadProfileImage(imagePath: "Profile", image: image, completion: {url in
            self.imageUploadResponse?(url)
        })
    }
    public func getMessages(){
        dummyMessageList.removeAll()
        FirebaseSerivce.getShared().getMessages(channelId: channelId, completion: {[weak self] (message,error,type) in
            if error == nil{
                if type! {
                    //self.messageList.removeAll(where: { $0.messageId == message!.messageId})
                } else {
                    self?.dummyMessageList.append(message!)
                }
            }
        }, completed: {[weak self] in
            self?.messages.removeAll()
            guard let messages = self?.dummyMessageList else {
                return
            }
            for msg in messages {
                self?.setDates(message: msg)
            }
            self?.messageResponse?(nil)
            //self.dummyMessageList.propertySort({$0.time})
        })
    }
    
    private func setDates(message: Message) {
        let timeInterval = Double((message.time))
        let date = Date(timeIntervalSince1970: timeInterval/1000)
        let fromatter = DateFormatter()
        fromatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
        let time = fromatter.string(from: date)
        var msg = message
        msg.date = fromatter.date(from: time)
        messages.append(msg)
    }
    public func getHeight(index: Int) -> CGFloat {
        let messageType = messages[index].messageType
        switch messageType {
        case .textMessage:
            return 0
        case .englishSign,.ArabicSign,.HebrewSign:
            return getHeightOfSignMessage(index: index)
        case .Image:
            return getHeightOfImageMessae()
        }
    }
    public func getHeightOfImageMessae() -> CGFloat {
        return 190
    }
    public func getHeightOfSignMessage(index: Int) -> CGFloat {
        let cellType = findDay(index: index)
        let count = getCount(index: index)
        switch cellType {
        case .Today:
            return CGFloat((count * 38) + 50)
        case .Same:
            return CGFloat((count * 38) + 30)
        case .Previous:
            return CGFloat((count * 38) + 50)
        }
    }
    public func configureCell(tableView: UITableView,indexPath: IndexPath) -> UITableViewCell {
        let messageType = messages[indexPath.row].messageType
        switch messageType {
        case .textMessage:
            return getTextCell(tableView: tableView, indexPath: indexPath)
        case .englishSign,.HebrewSign,.ArabicSign:
            return getSignCell(tableView: tableView, indexPath: indexPath)
        case .Image:
            return getImageCell(tableView: tableView, indexPath: indexPath)
        }
    }
    public func getTextCell(tableView: UITableView,indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConservationSecondTableViewCell", for: indexPath) as! ConservationSecondTableViewCell
        let cellType = findDay(index: indexPath.row)
        let conservationSecondCellViewModel = ConservationSecondCellViewModel(message: messages[indexPath.row], cellType: cellType)
        cell.configure(viewModel: conservationSecondCellViewModel)
        return cell
    }
    public func getImageCell(tableView: UITableView,indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageMessageTableViewCell", for: indexPath) as! ImageMessageTableViewCell
        let cellType = findDay(index: indexPath.row)
        let imageMessageCellViewModel = ImageMessageCellViewModel(message: messages[indexPath.row], cellType: cellType)
        cell.configure(imageMessagecellViewModel: imageMessageCellViewModel)
        return cell
    }
    public func getSignCell(tableView: UITableView,indexPath: IndexPath) -> UITableViewCell {
        CM.items = ["Sign English", "Sign Arabic", "Sign Hebrew"]
        let cellType = findDay(index: indexPath.row)
        switch cellType {
        case .Today,.Previous:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignDayTableViewCell", for: indexPath) as! SignDayTableViewCell
            let signDateCellViewModel = SignDayCellViewModel(message: messages[indexPath.row],cellType: cellType)
            cell.configure(signDayCellViewModel: signDateCellViewModel)
            cell.longPressResponse = {[weak self] (type) in
                guard let isFirstLong = self?.isFirstLong else {
                    return
                }
                if  isFirstLong {
                    self?.isFirstLong = false
                    if type == .Sender {
                        CM.showMenu(viewTargeted: cell.senderView, delegate: self!, animated: true, index: indexPath.row)
                    } else {
                        CM.showMenu(viewTargeted: cell.receiverView, delegate: self!, animated: true, index: indexPath.row)
                    }
                }
            }
            return cell
        case .Same:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignDateTableViewCell", for: indexPath) as! SignDateTableViewCell
            let signDateCellViewModel = SignDateCellViewModel(message: messages[indexPath.row])
            cell.configure(signDateCellViewModel: signDateCellViewModel)
            cell.longPressResponse = {[weak self] (type) in
                guard let isFirstLong = self?.isFirstLong else {
                    return
                }
                if isFirstLong {
                    self?.isFirstLong = false
                    if type == .Sender {
                        CM.showMenu(viewTargeted: cell.senderView, delegate: self!, animated: true, index: indexPath.row)
                    } else {
                        CM.showMenu(viewTargeted: cell.receiverView, delegate: self!, animated: true, index: indexPath.row)
                    }
                }
            }
            return cell
        }
    }
    public func configureCell(collectionView: UICollectionView,indexPath: IndexPath) -> TypeMessageCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeMessageCollectionViewCell", for: indexPath) as! TypeMessageCollectionViewCell
        let signMessage = signMessages[indexPath.row]
        cell.configure(signMessage: signMessage)
        return cell
    }
    public func addSignMessage(word: String,value: String?) {
        let signMessage = SignMessage(iconName: word, type: "Image", value: value)
        signMessages.append(signMessage)
    }
    public func inboxExist(id: String) {
        guard let userId = Auth.auth().currentUser?.uid else {
            return
        }
        FirebaseSerivce.getShared().getInboxExist(mineId: userId, receiverId: id, completion: {[weak self] inbox in
            self?.inboxResponse?(inbox)
        })
    }
    public func setInbox(inbox: Inbox) {
        FirebaseSerivce.getShared().addInbox(inbox: inbox)
    }
    public func sendMessage(text: String,inboxId: String,messageType: MessageType) {
        let time = Utils.getShared().getMillis()
        let id = Utils.getShared().randomString(length: 10)
        guard let user = StaticData.user else {
            return
        }
        let seenDict: [String:String] = [:]
        let message = Message(duration: nil, inboxId: inboxId, list: signMessages, message: text, messageId: id, messageType: messageType, seen: seenDict, senderId: user.id, senderName: user.name, sent: true, time: time, translationMap: ["Translation": getMessageType().rawValue])
        FirebaseSerivce.getShared().addMessage(message: message, channelId: self.channelId)
    }
    public func getMessageType() -> MessageType {
        switch keyboardType {
        case .Arabic:
            return .ArabicSign
        case .English:
            return .englishSign
        case .Text:
            return .textMessage
        case .Hebrew:
            return .HebrewSign
        case .Image:
            return .Image
        }
    }
    public func getMessage() -> String {
        var message = ""
        for signMessage in signMessages {
            if let value = signMessage.value {
                message = message + value
            } else {
                message = message + " "
            }
        }
        return message
    }
    func getCount(index: Int) -> Int {
        guard let signMessages = messages[index].list else {
            return 0
        }
        let count = (signMessages.count / 10) + 1
        return count
    }
    func findDay(index: Int) -> DateEnum {
        let calendar = Calendar.current
        let currentDate = calendar.startOfDay(for: Date())
        guard let secondDate = messages[index].date else {
            return .Today
        }
        let date1 = calendar.startOfDay(for: secondDate)
        if index == 0 {
            let components = calendar.dateComponents([.day], from: date1, to: currentDate)
            if components.day == 0 {
                return .Today
            } else {
                return .Previous
            }
        } else {
            guard let firstDate = messages[index - 1].date else {
                return .Today
            }
            // Replace the hour (time) of both dates with 00:00
            let date2 = calendar.startOfDay(for: firstDate)
            
            let components = calendar.dateComponents([.day], from: date1, to: date2)
            if components.day == 0 {
                return .Same
            } else {
                let components = calendar.dateComponents([.day], from: date1, to: currentDate)
                if components.day == 0 {
                    return .Today
                } else {
                    return .Previous
                }
            }
        }
    }
    private func translateMessage(index: Int,from: String,to: String) {
        let message  = messages[index]
        guard let text = message.message else {
            return
        }
        Translate.getShared().getTranslation(text: text, to: to, completion: {[weak self] (translation,error) in
            if error == nil {
                guard let translation = translation else {
                    return
                }
                self?.sortTranslation(translation: translation, to: to, index: index)
            } else {
                
            }
        })
    }
    private func sortTranslation(translation: Translation,to: String,index: Int) {
        var text = ""
        var type: MessageType = .ArabicSign
        if to == "en" {
            type = .englishSign
            guard let en = translation.translated_text.en else {
                return
            }
            text = en
        } else if to == "ar" {
            type = .ArabicSign
            guard let ar = translation.translated_text.ar else {
                return
            }
            text = ar
        } else {
            type = .HebrewSign
            guard let iw = translation.translated_text.iw else {
                return
            }
            text = iw
        }
        var signMessages: Array<SignMessage> = []
            let array = Array(text)
            if to == "ar" {
                let englishKey = ArabicKeys()
                for char in array {
                    let icon = englishKey.getIcon(word: String(char))
                    let signMessage = SignMessage(iconName: icon.rawValue, type: "Image", value: String(char))
                    signMessages.append(signMessage)
                }
            } else if to == "en" {
                let englishKey = EnglishKeys()
                for char in array {
                    let icon = englishKey.getIcon(word: String(char))
                    let signMessage = SignMessage(iconName: icon.rawValue, type: "Image", value: String(char))
                    signMessages.append(signMessage)
                }
            } else {
                let englishKey = HebrewKeys()
                for char in array {
                    let icon = englishKey.getIcon(word: String(char))
                    let signMessage = SignMessage(iconName: icon.rawValue, type: "Image", value: String(char))
                    signMessages.append(signMessage)
                }
            }
            DispatchQueue.main.async { [weak self] in
                self?.messages[index].messageType = type
                self?.messages[index].list?.removeAll()
                self?.messages[index].list?.append(contentsOf: signMessages)
                self?.messageResponse?(index)
            }
    }
    deinit {
        print("asad")
    }
}
enum KeyboardType {
    case Arabic
    case English
    case Hebrew
    case Text
    case Image
}
enum DateEnum {
    case Today
    case Same
    case Previous
}
enum LongPressType {
    case Sender
    case Receiver
}
extension ConversationViewModel: ContextMenuDelegate {
    func contextMenuDidSelect(_ contextMenu: ContextMenu, cell: ContextMenuCell, targetedView: UIView, didSelect item: ContextMenuItem, forRowAt index: Int, itemIndex: Int) -> Bool {
        let currentMessage = messages[itemIndex]
        if index == 0 {
            let type = currentMessage.messageType
            switch type {
            case .englishSign:
                break
            case .ArabicSign:
                translateMessage(index: itemIndex, from: "ar", to: "en")
                break
            case .HebrewSign:
                translateMessage(index: itemIndex, from: "iw", to: "en")
                break
            default:
                break
            }
        } else if index == 1 {
            let type = currentMessage.messageType
            switch type {
            case .englishSign:
                translateMessage(index: itemIndex, from: "en", to: "ar")
                break
            case .ArabicSign:
                break
            case .HebrewSign:
                translateMessage(index: itemIndex, from: "iw", to: "ar")
                break
            default:
                break
            }
        } else {
            let type = currentMessage.messageType
            switch type {
            case .englishSign:
                translateMessage(index: itemIndex, from: "en", to: "iw")
                break
            case .ArabicSign:
                translateMessage(index: itemIndex, from: "ar", to: "iw")
                break
            case .HebrewSign:
                break
            default:
                break
            }
        }
        return true
    }
    func contextMenuDidDeselect(_ contextMenu: ContextMenu, cell: ContextMenuCell, targetedView: UIView, didSelect item: ContextMenuItem, forRowAt index: Int) {
    }
    func contextMenuDidAppear(_ contextMenu: ContextMenu) {
        print("contextMenuDidAppear")
    }
    
    func contextMenuDidDisappear(_ contextMenu: ContextMenu) {
        isFirstLong = true
    }
    
}
