//
//  MessageCellViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 18/05/2022.
//

import Foundation
class MessageCellViewModel {
    private var inbox: Inbox!
    private var userId: String!
    init(inbox: Inbox) {
        self.inbox = inbox
    }
    func getInbox() -> Inbox {
        return inbox
    }
    func getUserName() -> String {
        if inbox.receiverId == userId {
            return inbox.senderName
        } else {
            return inbox.receiverName
        }
    }
    func getUserImage() -> String {
        if inbox.receiverId == userId {
            return inbox.senderImage
        } else {
            return inbox.receiverImage
        }
    }
}
