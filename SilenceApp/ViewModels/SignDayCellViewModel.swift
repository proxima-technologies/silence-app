//
//  SignDayCellViewModel.swift
//  SilenceApp
//
//  Created by Rana Asad on 24/05/2022.
//

import Foundation
import FirebaseAuth
class SignDayCellViewModel {
    public var isMine = false
    public var message: Message!
    public var cellType: DateEnum!
    init(message: Message,cellType: DateEnum) {
        self.message = message
        self.cellType = cellType
        checkUser()
    }
    func getSignMessage() -> Array<SignMessage> {
        guard let signMessage = message.list else {
            return []
        }
        return signMessage
    }
    func checkUser() {
        guard let id = Auth.auth().currentUser?.uid else {
            return
        }
        if message.senderId == id {
            isMine = true
        } else {
            isMine = false
        }
    }
    public func configureCell(collectionView: UICollectionView,indexPath: IndexPath) -> TypeMessageCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeMessageCollectionViewCell", for: indexPath) as! TypeMessageCollectionViewCell
        let signMessage = getSignMessage()[indexPath.row]
        cell.configure(signMessage: signMessage)
        return cell
    }
    func getWidthCount() -> Int {
        let count = getSignMessage().count
        var numberofItems: Int = 0
        if count < 10 {
            numberofItems = count
        } else {
            numberofItems = 10
        }
        return numberofItems
    }
}
