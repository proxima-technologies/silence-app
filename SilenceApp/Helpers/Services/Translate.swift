//
//  SwiftGoogleTranslate.swift
//  SilenceApp
//
//  Created by Rana Asad on 24/05/2022.
//

import Foundation
import Alamofire
class Translate {
    private static var shared: Translate?
    private let baseUrl = "https://nlp-translation.p.rapidapi.com/v1/translate"
    public static func getShared() -> Translate {
        if shared == nil {
            shared = Translate()
        }
        return shared!
    }
    func getTranslation(text: String,to: String,completion: @escaping((Translation?,String?) -> Void)) {
        let headers: HTTPHeaders = [
            "x-rapidapi-key": "c2117f70f6mshf2677a9a2dd3864p101c89jsn92b49d9f0692",
            "x-rapidapi-host": "nlp-translation.p.rapidapi.com"
        ]
        let parameters = ["text": text,"to":to]
        AF.request(baseUrl, parameters: parameters, headers: headers).responseDecodable(of: Translation.self) { response in
            print(response)
            switch response.result {
            case .success(let translation):
                completion(translation,nil)
                break
            case .failure(let error):
                print(error.localizedDescription)
                completion(nil,"")
                break
            }
        }
    }
}
