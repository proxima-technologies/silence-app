//
//  FirebaseService.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import Foundation
import FirebaseAuth
import FirebaseStorage
import FirebaseFirestore
import CodableFirebase
class FirebaseSerivce {
    private static var shared: FirebaseSerivce?
    private let db = Firestore.firestore()
    public static func getShared() -> FirebaseSerivce {
        if shared == nil {
            shared = FirebaseSerivce()
        }
        return shared!
    }
    func phoneAuthentication(phoneNumber: String,completion: @escaping((String?,String?) -> Void)) {
        PhoneAuthProvider.provider()
          .verifyPhoneNumber(phoneNumber, uiDelegate: nil) { verificationID, error in
              if let err = error {
                  completion(nil,err.localizedDescription)
                return
              }
              completion(verificationID,nil)
          }
    }
    func authenticateOTP(otp: String,verificationId: String,completion: @escaping((String?,String?) -> Void)) {
        let credential = PhoneAuthProvider.provider().credential(
          withVerificationID: verificationId,
          verificationCode: otp
        )
        Auth.auth().signIn(with: credential) { authResult, error in
            if let err = error {
                completion(nil,err.localizedDescription)
            }
            if let authRes = authResult {
                completion(authRes.user.uid,nil)
            }
        }
    }
    func getUserDetails(id:String,completion:@escaping((User?,String?)->Void)){
        db.collection("Users").document(id).getDocument(completion:{(document,error) in
            if error == nil{
                if document!.data() == nil {
                    completion(nil, "")
                } else {
                    let documentObj=try! FirestoreDecoder().decode(User.self, from: document!.data()!)
                    completion(documentObj,nil)
                }
                
                
            }else{
                completion(nil,error!.localizedDescription)
            }
        })
    }
    func getAllUsers(completion:@escaping((Array<User>?,String?)->Void)){
        var array: Array<User> = []
        db.collection("Users").getDocuments(completion:{(snapshot,error) in
            if error == nil{
                if let documents = snapshot?.documents {
                    for document in documents {
                        let documentObj=try! FirestoreDecoder().decode(User.self, from: document.data())
                        array.append(documentObj)
                    }
                    completion(array,nil)
                } else {
                    completion(nil, "")
                }
            }else{
                completion(nil,error!.localizedDescription)
            }
        })
    }
    func uploadProfileImage(imagePath:String,image:UIImage,completion: @escaping (_ url: String?) -> Void) {
        let storageRef = Storage.storage().reference().child(imagePath).child("\(Utils.getShared().randomString(length: 25)).jpeg")
        if let uploadData = image.jpegData(compressionQuality: 0.5) {
            storageRef.putData(uploadData, metadata: nil) { (metadata, error) in
                if error != nil {
                    print("error")
                    completion(nil)
                } else {
                    storageRef.downloadURL(completion: { (url, error) in
                        completion(url?.absoluteString)
                    })
                }
            }
        }
    }
    func signUpUser(user: User,completion:@escaping((String?)->Void)){
        let docData = try! FirestoreEncoder().encode(user)
        db.collection("Users").document(user.id).setData(docData,merge:true){ err in
            if let err = err{
                completion("Error writing document: \(err)")
            }
            else{
                completion(nil)
            }
        }
    }
    func getInbox(id: String,completion:@escaping((Inbox?,String?,Bool?)->Void)) {
        db.collection("Inbox").whereField("joinedList", arrayContains: id).addSnapshotListener{(snap,error) in
               if error == nil{
                   for document in snap!.documentChanges{
                       let documentObj=try! FirestoreDecoder().decode(Inbox.self, from: document.document.data())
                       if document.type == .added {
                           completion(documentObj,nil,false)
                       } else if document.type == .modified {
                           completion(documentObj,nil,true)
                       }
                   }
               }else{
                   completion(nil,error!.localizedDescription,nil)
               }
           }
    }
    func getInboxExist(mineId: String,receiverId: String,completion:@escaping((Inbox?)->Void)) {
        db.collection("Inbox").whereField("joinedList", arrayContains: [mineId,receiverId]).getDocuments(completion: { snap,error in
            if let err = error {
                completion(nil)
            } else {
                if let documents = snap?.documents {
                    if documents.count > 0 {
                        for document in documents {
                            let documentObj=try! FirestoreDecoder().decode(Inbox.self, from: document.data())
                            completion(documentObj)
                        }
                    } else {
                        completion(nil)
                    }
                } else {
                    completion(nil)
                }
            }
        })
    }
    func documentExist(id: String,completion:@escaping((Bool)->Void)) {
        db.collection("Chat").document(id).getDocument(completion: { document,error in
            if let error = error {
                completion(false)
            } else {
                if let documnet = document?.exists {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        })
    }
    func addInbox(inbox: Inbox) {
        let docData = try! FirestoreEncoder().encode(inbox)
        db.collection("Inbox").document(inbox.id).setData(docData, merge: true)
    }
    func addMessage(message: Message,channelId: String) {
        let docData = try! FirestoreEncoder().encode(message)
        db.collection("Chat").document(channelId).collection("Messages").document(message.messageId).setData(docData, merge: true)
    }
    func getMessages(channelId:String,completion:@escaping((Message?,String?,Bool?)->Void),completed:@escaping(()->Void)){
            db.collection("Chat").document(channelId).collection("Messages").order(by: "time",descending: false).addSnapshotListener(includeMetadataChanges:true, listener: { snap,error in
                if error == nil{
                    for document in snap!.documentChanges{
                        let documentObj=try! FirestoreDecoder().decode(Message.self, from: document.document.data())
                        if document.type == .modified {
                            completion(documentObj,nil,true)
                        } else if document.type == .added {
                            completion(documentObj,nil,false)
                        } else {
                            completion(documentObj,nil,true)
                        }
                    }
                    completed()
                }else{
                    completion(nil,error!.localizedDescription,nil)
                }
            })
        }
    // MARK: -  block Functions
    func blockUser(body1:Block,body2:Block,userId: String) {
        let data1 = try! FirestoreEncoder().encode(body1)
        let data2 = try! FirestoreEncoder().encode(body2)
        db.collection("Users").document(Auth.auth().currentUser!.uid).collection("BlockedCollection").document(userId).setData(data1,merge: true)
        db.collection("Users").document(userId).collection("BlockedCollection").document(Auth.auth().currentUser!.uid).setData(data2,merge: true)
    }
   
    func unBlockUser(userId: String) {
        db.collection("Users").document(Auth.auth().currentUser!.uid).collection("BlockedCollection").document(userId).delete()
        db.collection("Users").document(userId).collection("BlockedCollection").document(Auth.auth().currentUser!.uid).delete()
    }
    func getAllBlockList(completion:@escaping([Block]?,String?)->()) {
        var data = [Block]()
        db.collection("Users").document(Auth.auth().currentUser!.uid).collection("BlockedCollection").getDocuments{ response, error in
            if error ==  nil {
                for doc in response!.documents {
                    let documentObj=try! FirestoreDecoder().decode(Block.self, from: doc.data())
                    data.append(documentObj)
                }
                completion(data,nil)
              
            } else {
                completion(nil,error!.localizedDescription)
            }
        }
    }
}
