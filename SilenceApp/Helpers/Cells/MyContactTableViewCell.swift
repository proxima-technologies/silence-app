//
//  MyContactTableViewCell.swift
//  SilenceApp
//
//  Created by Rana Asad on 19/05/2022.
//

import UIKit
import SDWebImage
class MyContactTableViewCell: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewDecorator()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCircleCornerRadius(view: userImageView)
    }
    func configure(contactCellViewModel: ContactCellViewModel) {
        let user = contactCellViewModel.getUser()
        usernameLabel.text = user.name
        statusLabel.text = user.status
        guard let url = URL(string: user.imageUrl) else {
            return
        }
        userImageView.sd_setImage(with: url)
    }

}
