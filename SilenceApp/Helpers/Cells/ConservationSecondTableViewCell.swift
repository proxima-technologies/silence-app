//
//  ConversationSecondTableViewCell.swift
//  SilenceApp
//
//  Created by Rana Asad on 26/05/2022.
//

import Foundation
import UIKit

class ConservationSecondTableViewCell: UITableViewCell {
    @IBOutlet weak var senderView: UIView!
    @IBOutlet weak var senderLabel: CopyableLabel!
    @IBOutlet weak var receivingView: UIView!
    @IBOutlet weak var receivingLabel: CopyableLabel!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var receivingTimeLabel: UILabel!
    var forwordTapCallBack: (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        viewDecorator()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view:  senderView)
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: receivingView)
    }
    public func configure(viewModel: ConservationSecondCellViewModel) {
        guard let message = viewModel.message.message else {
            return
        }
        guard let date = viewModel.message.date else {
            return
        }
        if viewModel.isMine {
            timeLabel.isHidden = false
            receivingLabel.isHidden = true
            receivingView.isHidden = true
            senderView.isHidden = false
            senderLabel.text = message
            timeLabel.text = date.toString(withFormat: "HH:mm")
        } else {
            timeLabel.isHidden = false
            receivingLabel.isHidden = true
            receivingView.isHidden = true
            senderView.isHidden = false
            receivingLabel.text = message
            receivingTimeLabel.text = date.toString(withFormat: "HH:mm")
        }
        switch viewModel.cellType {
        case .Today:
            todayLabel.isHidden = false
            todayLabel.text = "Today"
            break
        case .Same:
             todayLabel.isHidden = true
            break
        case .Previous:
            todayLabel.isHidden = false
            todayLabel.text = date.toString(withFormat: "yyyy-MM-dd")
            break
        default:
            break
        }
    }
    
}
