//
//  TypeMessageCollectionViewCell.swift
//  SilenceApp
//
//  Created by Rana Asad on 23/05/2022.
//

import UIKit

class TypeMessageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var messageIconImageView: UIImageView!
    
    func configure(signMessage: SignMessage) {
        if signMessage.type == "space" {
            messageIconImageView.image = UIImage(named: "")
        } else {
            messageIconImageView.image = UIImage(named: "")
            messageIconImageView.image = UIImage(named: signMessage.iconName!)
        }
    }
}
