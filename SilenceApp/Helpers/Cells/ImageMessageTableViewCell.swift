//
//  ImageMessageTableViewCell.swift
//  SilenceApp
//
//  Created by Rana Asad on 25/05/2022.
//

import UIKit
import SDWebImage
class ImageMessageTableViewCell: UITableViewCell {
    @IBOutlet weak var senderDateLabel: UILabel!
    @IBOutlet weak var receiverDateLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var senderImageView: UIImageView!
    @IBOutlet weak var receiverImageView: UIImageView!
    
    private var imageMessagecellViewModel: ImageMessageCellViewModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        viewDecorator()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: senderImageView)
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: receiverImageView)
    }
    public func configure(imageMessagecellViewModel: ImageMessageCellViewModel) {
        self.imageMessagecellViewModel = imageMessagecellViewModel
        guard let message = imageMessagecellViewModel.message.message else {
            return
        }
        guard let url = URL(string: message) else {
            return
        }
        guard let date = imageMessagecellViewModel.message.date else {
            return
        }
        if imageMessagecellViewModel.isMine {
            senderDateLabel.isHidden = false
            receiverDateLabel.isHidden = true
            receiverImageView.isHidden = true
            senderImageView.isHidden = false
            senderImageView.sd_setImage(with: url)
            senderDateLabel.text = date.toString(withFormat: "HH:mm")
        } else {
            senderDateLabel.isHidden = true
            receiverDateLabel.isHidden = false
            receiverImageView.isHidden = false
            senderImageView.isHidden = true
            senderImageView.sd_setImage(with: url)
            receiverDateLabel.text = date.toString(withFormat: "HH:mm")
        }
        switch imageMessagecellViewModel.cellType {
        case .Today:
            dayLabel.isHidden = false
            dayLabel.text = "Today"
            break
        case .Same:
            dayLabel.isHidden = true
            break
        case .Previous:
            dayLabel.isHidden = false
            dayLabel.text = date.toString(withFormat: "yyyy-MM-dd")
            break
        default:
            break
        }
    }

}
