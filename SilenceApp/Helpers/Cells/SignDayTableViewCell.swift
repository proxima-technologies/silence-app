//
//  SignDayTableViewCell.swift
//  SilenceApp
//
//  Created by Rana Asad on 24/05/2022.
//

import UIKit
class SignDayTableViewCell: UITableViewCell {
    @IBOutlet weak var senderView: UIView!
    @IBOutlet weak var senderDateLabel: UILabel!
    @IBOutlet weak var receiverView: UIView!
    @IBOutlet weak var receiverDateLabel: UILabel!
    @IBOutlet weak var senderCollectionView: UICollectionView!
    @IBOutlet weak var receiverCollectionView: UICollectionView!
    @IBOutlet weak var dayLabel: UILabel!
    private var signDayCellViewModel: SignDayCellViewModel?
    public var longPressResponse: ((LongPressType)->())?
    private var isFirst = true
    override func awakeFromNib() {
        super.awakeFromNib()
        viewDecorator()
        self.senderView.isHidden = true
        self.receiverView.isHidden = true
        addGesture()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func addGesture() {
        senderView.isUserInteractionEnabled = true
        receiverView.isUserInteractionEnabled = true
        senderView.tag = 1
        receiverView.tag = 2
        senderView.addGestureRecognizer(getGesture())
        receiverView.addGestureRecognizer(getGesture())
        
    }
    private func getGesture() -> UILongPressGestureRecognizer {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
        return longPressRecognizer
    }
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        switch sender.view?.tag {
        case 1:
            longPressResponse?(.Sender)
            break
        case 2:
            longPressResponse?(.Receiver)
            break
        default:
            break
        }
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: senderView)
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: receiverView)
    }
    private func setupCollectionView() {
        let snCollectionViewLayout = SNCollectionViewLayout()
        snCollectionViewLayout.fixedDivisionCount = UInt(10)
        snCollectionViewLayout.delegate = self
        senderCollectionView.delegate = self
        senderCollectionView.dataSource = self
        senderCollectionView.collectionViewLayout = snCollectionViewLayout
        receiverCollectionView.delegate = self
        receiverCollectionView.dataSource = self
        receiverCollectionView.collectionViewLayout = snCollectionViewLayout
    }
    func configure(signDayCellViewModel: SignDayCellViewModel) {
        initCollectionView(signDayCellViewModel: signDayCellViewModel)
        if isFirst {
            isFirst = false
            setupCollectionView()
        } else {
            senderCollectionView.reloadData()
            receiverCollectionView.reloadData()
        }
    }
    private func initCollectionView(signDayCellViewModel: SignDayCellViewModel) {
            self.signDayCellViewModel = signDayCellViewModel
        guard let date = signDayCellViewModel.message.date else {
            return
        }
            if signDayCellViewModel.isMine {
                senderView.isHidden = false
                receiverView.isHidden = true
                senderDateLabel.isHidden = false
                receiverDateLabel.isHidden = true
                senderDateLabel.text = date.toString(withFormat: "HH:mm")
            } else {
                senderView.isHidden = true
                receiverView.isHidden = false
                senderDateLabel.isHidden = true
                receiverDateLabel.isHidden = false
                receiverDateLabel.text = date.toString(withFormat: "HH:mm")
            }
        switch signDayCellViewModel.cellType {
        case .Today:
            dayLabel.text = "Today"
            break
        case .Same:
            break
        case .Previous:
            dayLabel.text = date.toString(withFormat: "yyyy-MM-dd")
            break
        default:
            break
        }
    }
}
extension SignDayTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,SNCollectionViewLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let signMessage = signDayCellViewModel?.getSignMessage() else {
            return 0
        }
        return signMessage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = signDayCellViewModel?.configureCell(collectionView: collectionView, indexPath: indexPath) else {
            return UICollectionViewCell()
        }
        return cell
    }
    func scaleForItem(inCollectionView collectionView: UICollectionView, withLayout layout: UICollectionViewLayout, atIndexPath indexPath: IndexPath) -> UInt {
        return 1
    }
}
