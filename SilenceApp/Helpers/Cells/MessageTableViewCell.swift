//
//  MessageTableViewCell.swift
//  SilenceApp
//
//  Created by Rana Asad on 18/05/2022.
//

import UIKit
import SDWebImage
class MessageTableViewCell: UITableViewCell {
    @IBOutlet weak var unreadView: UIView!
    @IBOutlet weak var unreadLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewDecorator()
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCircleCornerRadius(view: unreadView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: userImageView)
    }
    func configure(messageCellViewModel: MessageCellViewModel) {
        let inbox = messageCellViewModel.getInbox()
        if inbox.isRead {
            unreadView.isHidden = false
        } else {
            unreadView.isHidden = true
        }
        userNameLabel.text = messageCellViewModel.getUserName()
        guard let imageUrl = URL(string: messageCellViewModel.getUserImage()) else {
            return
        }
        userImageView.sd_setImage(with: (imageUrl))
        messageLabel.text = inbox.message
        timeLabel.text = Utils.getShared().getNoOfHours(time: inbox.time)
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
