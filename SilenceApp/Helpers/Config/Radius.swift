//
//  Radius.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import UIKit
enum RadiusValue: CGFloat {
    case ButtonRadius = 5
    case TextFieldRadius = 8
    case CardRadius = 12
}
enum Radius {
    case ButtonRadius
    case TextFieldRadius
    case CardRadius
}
extension Radius {
    var value: CGFloat {
        get {
            switch self {
            case .ButtonRadius:
                return CGFloat(RadiusValue.ButtonRadius.rawValue)
            case .TextFieldRadius:
                return CGFloat(RadiusValue.TextFieldRadius.rawValue)
            case .CardRadius:
                return CGFloat(RadiusValue.CardRadius.rawValue)
            }
        }
    }
}

