//
//  EnglishKeyboard.swift
//  SilenceApp
//
//  Created by Rana Asad on 23/05/2022.
//

import UIKit

class EnglishKeyboard: UIView {
    @IBOutlet weak var icA: UIImageView!
    @IBOutlet weak var icB: UIImageView!
    @IBOutlet weak var icC: UIImageView!
    @IBOutlet weak var icD: UIImageView!
    @IBOutlet weak var icE: UIImageView!
    @IBOutlet weak var icF: UIImageView!
    @IBOutlet weak var icG: UIImageView!
    @IBOutlet weak var icH: UIImageView!
    @IBOutlet weak var icI: UIImageView!
    @IBOutlet weak var icJ: UIImageView!
    @IBOutlet weak var icK: UIImageView!
    @IBOutlet weak var icL: UIImageView!
    @IBOutlet weak var icM: UIImageView!
    @IBOutlet weak var icN: UIImageView!
    @IBOutlet weak var icO: UIImageView!
    @IBOutlet weak var icP: UIImageView!
    @IBOutlet weak var icQ: UIImageView!
    @IBOutlet weak var icR: UIImageView!
    @IBOutlet weak var icS: UIImageView!
    @IBOutlet weak var icT: UIImageView!
    @IBOutlet weak var icU: UIImageView!
    @IBOutlet weak var icV: UIImageView!
    @IBOutlet weak var icW: UIImageView!
    @IBOutlet weak var icX: UIImageView!
    @IBOutlet weak var icY: UIImageView!
    @IBOutlet weak var icBack: UIImageView!
    @IBOutlet weak var icSpace: UIImageView!
    @IBOutlet weak var icZ: UIImageView!
    
    public var englishResponse: ((EnglishWords,String?)->())?
    func addGesture() {
        icA.tag = 1
        icB.tag = 2
        icC.tag = 3
        icD.tag = 4
        icE.tag = 5
        icF.tag = 6
        icG.tag = 7
        icH.tag = 8
        icI.tag = 9
        icJ.tag = 10
        icK.tag = 11
        icL.tag = 12
        icM.tag = 13
        icN.tag = 14
        icO.tag = 15
        icP.tag = 16
        icQ.tag = 17
        icR.tag = 18
        icS.tag = 19
        icT.tag = 20
        icU.tag = 21
        icV.tag = 22
        icW.tag = 23
        icX.tag = 24
        icY.tag = 25
        icZ.tag = 26
        icSpace.tag = 27
        icBack.tag = 28
        icA.addGestureRecognizer(getGesture())
        icB.addGestureRecognizer(getGesture())
        icC.addGestureRecognizer(getGesture())
        icD.addGestureRecognizer(getGesture())
        icE.addGestureRecognizer(getGesture())
        icF.addGestureRecognizer(getGesture())
        icG.addGestureRecognizer(getGesture())
        icH.addGestureRecognizer(getGesture())
        icI.addGestureRecognizer(getGesture())
        icJ.addGestureRecognizer(getGesture())
        icK.addGestureRecognizer(getGesture())
        icL.addGestureRecognizer(getGesture())
        icM.addGestureRecognizer(getGesture())
        icN.addGestureRecognizer(getGesture())
        icO.addGestureRecognizer(getGesture())
        icP.addGestureRecognizer(getGesture())
        icQ.addGestureRecognizer(getGesture())
        icR.addGestureRecognizer(getGesture())
        icS.addGestureRecognizer(getGesture())
        icT.addGestureRecognizer(getGesture())
        icU.addGestureRecognizer(getGesture())
        icV.addGestureRecognizer(getGesture())
        icW.addGestureRecognizer(getGesture())
        icX.addGestureRecognizer(getGesture())
        icY.addGestureRecognizer(getGesture())
        icZ.addGestureRecognizer(getGesture())
        icSpace.addGestureRecognizer(getGesture())
        icBack.addGestureRecognizer(getGesture())
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc private func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag
        {
        case 1:
            englishResponse?(.A,"a")
            break
        case 2:
            englishResponse?(.B,"b")
            break
        case 3:
            englishResponse?(.C,"c")
            break
        case 4:
            englishResponse?(.D,"d")
            break
        case 5:
            englishResponse?(.E,"e")
            break
        case 6:
            englishResponse?(.F,"f")
            break
        case 7:
            englishResponse?(.G,"g")
            break
        case 8:
            englishResponse?(.H,"h")
            break
        case 9:
            englishResponse?(.I,"i")
            break
        case 10:
            englishResponse?(.J,"j")
            break
        case 11:
            englishResponse?(.K,"k")
            break
        case 12:
            englishResponse?(.L,"l")
            break
        case 13:
            englishResponse?(.M,"m")
            break
        case 14:
            englishResponse?(.N,"n")
            break
        case 15:
            englishResponse?(.O,"o")
            break
        case 16:
            englishResponse?(.P,"p")
            break
        case 17:
            englishResponse?(.Q,"q")
            break
        case 18:
            englishResponse?(.R,"r")
            break
        case 19:
            englishResponse?(.S,"s")
            break
        case 20:
            englishResponse?(.T,"t")
            break
        case 21:
            englishResponse?(.U,"u")
            break
        case 22:
            englishResponse?(.V,"v")
            break
        case 23:
            englishResponse?(.W,"w")
            break
        case 24:
            englishResponse?(.X,"x")
            break
        case 25:
            englishResponse?(.Y,"y")
            break
        case 26:
            englishResponse?(.Z,"z")
            break
        case 27:
            englishResponse?(.s,nil)
            break
        case 28:
            englishResponse?(.b,nil)
            break
        default:
            break
        }
    }
    
}
enum EnglishWords: String {
    case A = "ic_a"
    case B = "ic_b"
    case C = "ic_c"
    case D = "ic_d"
    case E = "ic_e"
    case F = "ic_f"
    case G = "ic_g"
    case H = "ic_h"
    case I = "ic_i"
    case J = "ic_j"
    case K = "ic_k"
    case L = "ic_l"
    case M = "ic_m"
    case N = "ic_n"
    case O = "ic_o"
    case P = "ic_p"
    case Q = "ic_q"
    case R = "ic_r"
    case S = "ic_s"
    case T = "ic_t"
    case U = "ic_u"
    case V = "ic_v"
    case W = "ic_w"
    case X = "ic_x"
    case Y = "ic_y"
    case Z = "ic_z"
    case b = "back"
    case s = "space"
}
class EnglishKeys {
    func getIcon(word: String) -> EnglishWords {
        switch word {
        case "a":
            return .A
        case "b":
            return .B
        case "c":
            return .C
        case "d":
            return .D
        case "e":
            return .E
        case "f":
            return .F
        case "g":
            return .G
        case "h":
            return .H
        case "i":
            return .I
        case "j":
            return .J
        case "k":
            return .K
        case "l":
            return .L
        case "m":
            return .M
        case "n":
            return .N
        case "o":
            return .O
        case "p":
            return .P
        case "q":
            return .Q
        case "r":
            return .R
        case "s":
            return .S
        case "t":
            return .T
        case "u":
            return .U
        case "v":
            return .V
        case "x":
            return .X
        case "y":
            return .Y
        case "z":
            return .Z
        case " ":
            return .s
        default:
            return .s
        }
    }
}
