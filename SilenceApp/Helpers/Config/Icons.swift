//
//  Icons.swift
//  SilenceApp
//
//  Created by Rana Asad on 19/05/2022.
//

import Foundation
import UIKit
class Icons {
    public static let checkIcon = UIImage(named: "ic_check")
    public static let editIcon = UIImage(named: "ic_edit")
}
