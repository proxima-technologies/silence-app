//
//  ArabicKeyboard.swift
//  SilenceApp
//
//  Created by Tayyab Mubeen on 24/05/2022.
//

import UIKit

class ArabicKeyboard: UIView {
    @IBOutlet weak var icAleph: UIImageView!
    @IBOutlet weak var icBaa: UIImageView!
    @IBOutlet weak var icTaa: UIImageView!
    @IBOutlet weak var icThaa: UIImageView!
    @IBOutlet weak var icJeem: UIImageView!
    @IBOutlet weak var icHaa: UIImageView!
    @IBOutlet weak var icKhaa: UIImageView!
    @IBOutlet weak var icDaal: UIImageView!
    @IBOutlet weak var icDhaal: UIImageView!
    @IBOutlet weak var icRaa: UIImageView!
    @IBOutlet weak var icZaa: UIImageView!
    @IBOutlet weak var icSeen: UIImageView!
    @IBOutlet weak var icSheen: UIImageView!
    @IBOutlet weak var icSaad: UIImageView!
    @IBOutlet weak var icDaad: UIImageView!
    @IBOutlet weak var icTaaa: UIImageView!
    @IBOutlet weak var icZaaa: UIImageView!
    @IBOutlet weak var icEin: UIImageView!
    @IBOutlet weak var icGhein: UIImageView!
    @IBOutlet weak var icFaa: UIImageView!
    @IBOutlet weak var icQaaf: UIImageView!
    @IBOutlet weak var icKaaf: UIImageView!
    @IBOutlet weak var icLaam: UIImageView!
    @IBOutlet weak var icMeem: UIImageView!
    @IBOutlet weak var icNoon: UIImageView!
    @IBOutlet weak var icHaaaa: UIImageView!
    @IBOutlet weak var icWaw: UIImageView!
    @IBOutlet weak var icYaa: UIImageView!
    @IBOutlet weak var icYaa_hamza: UIImageView!
    @IBOutlet weak var icWaw_Hamza: UIImageView!
    @IBOutlet weak var icSpace: UIImageView!
    @IBOutlet weak var icZ: UIImageView!
    
    public var ArabicResponse: ((ArabicWords,String?)->())?
    func addGesture() {
        icAleph.tag = 1
        icBaa.tag = 2
        icTaa.tag = 3
        icThaa.tag = 4
        icJeem.tag = 5
        icHaa.tag = 6
        icKhaa.tag = 7
        icDaal.tag = 8
        icDhaal.tag = 9
        icRaa.tag = 10
        icZaa.tag = 11
        icSeen.tag = 12
        icSheen.tag = 13
        icSaad.tag = 14
        icDaad.tag = 15
        icTaaa.tag = 16
        icZaaa.tag = 17
        icEin.tag = 18
        icGhein.tag = 19
        icFaa.tag = 20
        icQaaf.tag = 21
        icKaaf.tag = 22
        icLaam.tag = 23
        icMeem.tag = 24
        icNoon.tag = 25
        icHaaaa.tag = 26
        icWaw.tag = 27
        icWaw_Hamza.tag = 28
        icYaa.tag = 29
        icYaa_hamza.tag = 30
        icSpace.tag = 31
        icZ.tag = 32
        
        icAleph.addGestureRecognizer(getGesture())
        icBaa.addGestureRecognizer(getGesture())
        icTaa.addGestureRecognizer(getGesture())
        icThaa.addGestureRecognizer(getGesture())
        icJeem.addGestureRecognizer(getGesture())
        icHaa.addGestureRecognizer(getGesture())
        icKhaa.addGestureRecognizer(getGesture())
        icDaal.addGestureRecognizer(getGesture())
        icDhaal.addGestureRecognizer(getGesture())
        icRaa.addGestureRecognizer(getGesture())
        icZaa.addGestureRecognizer(getGesture())
        icSeen.addGestureRecognizer(getGesture())
        icSheen.addGestureRecognizer(getGesture())
        icSaad.addGestureRecognizer(getGesture())
        icDaad.addGestureRecognizer(getGesture())
        icTaaa.addGestureRecognizer(getGesture())
        icZaaa.addGestureRecognizer(getGesture())
        icEin.addGestureRecognizer(getGesture())
        icGhein.addGestureRecognizer(getGesture())
        icFaa.addGestureRecognizer(getGesture())
        icQaaf.addGestureRecognizer(getGesture())
        icKaaf.addGestureRecognizer(getGesture())
        icLaam.addGestureRecognizer(getGesture())
        icMeem.addGestureRecognizer(getGesture())
        icNoon.addGestureRecognizer(getGesture())
        icHaaaa.addGestureRecognizer(getGesture())
        icWaw.addGestureRecognizer(getGesture())
        icYaa.addGestureRecognizer(getGesture())
        icYaa_hamza.addGestureRecognizer(getGesture())
        icWaw_Hamza.addGestureRecognizer(getGesture())
        icSpace.addGestureRecognizer(getGesture())
        icZ.addGestureRecognizer(getGesture())
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc private func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag
        {
        case 1:
            ArabicResponse?(.Aleph,"ا")
            break
        case 2:
            ArabicResponse?(.Baa,"ب")
            break
        case 3:
            ArabicResponse?(.Taa,"ت")
            break
        case 4:
            ArabicResponse?(.Thaa,"ث")
            break
        case 5:
            ArabicResponse?(.Jeem,"ج")
            break
        case 6:
            ArabicResponse?(.Haa,"ح")
            break
        case 7:
            ArabicResponse?(.Khaa,"خ")
            break
        case 8:
            ArabicResponse?(.Daal,"د")
            break
        case 9:
            ArabicResponse?(.Dhaal,"ذ")
            break
        case 10:
            ArabicResponse?(.Raa,"ر")
            break
        case 11:
            ArabicResponse?(.Zaa,"ز")
            break
        case 12:
            ArabicResponse?(.Seen,"س")
            break
        case 13:
            ArabicResponse?(.Sheen,"ش")
            break
        case 14:
            ArabicResponse?(.Saad,"ص")
            break
        case 15:
            ArabicResponse?(.Daad,"ض")
            break
        case 16:
            ArabicResponse?(.Taaa,"ط")
            break
        case 17:
            ArabicResponse?(.Zaaa,"ظ")
            break
        case 18:
            ArabicResponse?(.Ein,"ع")
            break
        case 19:
            ArabicResponse?(.Ghein,"غ")
            break
        case 20:
            ArabicResponse?(.Faa,"ف")
            break
        case 21:
            ArabicResponse?(.Qaaf,"ق")
            break
        case 22:
            ArabicResponse?(.Kaaf,"ك")
            break
        case 23:
            ArabicResponse?(.Laam,"ل")
            break
        case 24:
            ArabicResponse?(.Meem,"م")
            break
        case 25:
            ArabicResponse?(.Noon,"ن")
            break
        case 26:
            ArabicResponse?(.Haaaa,"ة")
            break
        case 27:
            ArabicResponse?(.Waw,"و")
            break
        case 28:
            ArabicResponse?(.Waw_hamza,"ؤ")
            break
        case 29:
            ArabicResponse?(.Yaa,"ى")
            break
        case 30:
            ArabicResponse?(.Yaa_hamza,"ي")
            break
        case  31:
            ArabicResponse?(.space," ")
            break
        case 32:
            ArabicResponse?(.Z,"")
            break
        default:
            break
        }
    }
}
enum ArabicWords: String {
    case Aleph = "ic_aleph"
    case Baa = "ic_baa"
    case Taa = "ic_taa"
    case Thaa = "ic_thaa"
    case Jeem = "ic_jeem"
    case Haa = "ic_haa"
    case Khaa = "ic_khaa"
    case Daal = "ic_daal"
    case Dhaal = "ic_dhaal"
    case Raa = "ic_raa"
    case Zaa = "ic_zaa"
    case Seen = "ic_seen"
    case Sheen = "ic_sheen"
    case Saad = "ic_saad"
    case Daad = "ic_daad"
    case Taaa = "ic_taaa"
    case Zaaa = "ic_zaaa"
    case Ein = "ic_ein"
    case Ghein = "ic_ghein"
    case Faa = "ic_faa"
    case Qaaf = "ic_qaaf"
    case Kaaf = "ic_kaaf"
    case Laam = "ic_laam"
    case Meem = "ic_meem"
    case Noon = "ic_noon"
    case Haaaa = "ic_taa_marbuuta"
    case Waw = "ic_waw"
    case Waw_hamza = "ic_waw_ hamza"
    case Yaa = "ic_yaa"
    case Yaa_hamza = "ic_yaa_hamza"
    case space = "ic_space"
    case Z = "ic_cross"
}
class ArabicKeys {
    func getIcon(word: String) -> ArabicWords {
        switch word {
        case "ا":
            return .Aleph
        case "ب":
            return .Baa
        case "ت":
            return .Taa
        case "ث":
            return .Thaa
        case "ج":
            return .Jeem
        case "ح":
            return .Haa
        case "خ":
            return .Khaa
        case "د":
            return .Daal
        case "ذ":
            return .Dhaal
        case "ر":
            return .Raa
        case "ز":
            return .Zaa
        case "س":
            return .Seen
        case "ش":
            return .Sheen
        case "ص":
            return .Saad
        case "ض":
            return .Daad
        case "ط":
            return .Taaa
        case "ظ":
            return .Zaaa
        case "ع":
            return .Ein
        case "غ":
            return .Ghein
        case "ف":
            return .Faa
        case "ق":
            return .Qaaf
        case "ك":
            return .Kaaf
        case "ل":
            return .Laam
        case "م":
            return .Meem
        case "ن":
            return .Noon
        case "ة":
            return .Haaaa
        case "و":
            return .Waw
        case "ؤ":
            return .Waw_hamza
        case "ى":
            return .Yaa
        case "ي":
            return .Yaa_hamza
        case " ":
            return .space
        case "z":
            return .Z
        default:
            return .space
        }
    }
}
