//
//  Colors.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import UIKit
enum ColorsName: String {
    case primaryColor = "PrimaryColor"
    case accentColor = "AccentColor"
    case borderColor = "GrayColor"
    case textViewBorderColor = "GrayBorderColor"
}
enum Colors {
    case primaryColor
    case accentColor
    case borderColor
    case textViewBorderColor
}
extension Colors {
    var value: UIColor {
        get {
            switch self {
            case .primaryColor:
                return UIColor(named: ColorsName.primaryColor.rawValue)!
            case .accentColor:
                return UIColor(named: ColorsName.accentColor.rawValue)!
            case .borderColor:
                return UIColor(named: ColorsName.borderColor.rawValue)!
            case .textViewBorderColor:
                return UIColor(named: ColorsName.textViewBorderColor.rawValue)!
            }
        }
    }
}

