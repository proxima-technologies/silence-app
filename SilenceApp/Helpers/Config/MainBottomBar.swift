//
//  MainBottomBar.swift
//  SilenceApp
//
//  Created by Rana Asad on 17/05/2022.
//

import UIKit

class MainBottomBar: UIView {
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var messageMiniView: UIView!
    @IBOutlet weak var cameraMiniView: UIView!
    @IBOutlet weak var profileMiniView: UIView!
    
}
