//
//  HebrewKeyboard.swift
//  SilenceApp
//
//  Created by Tayyab Mubeen on 25/05/2022.
//

import UIKit

class HebrewKeyboard: UIView {
    
    @IBOutlet weak var ic_A: UIImageView!
    @IBOutlet weak var ic_B: UIImageView!
    @IBOutlet weak var ic_C: UIImageView!
    @IBOutlet weak var ic_D: UIImageView!
    @IBOutlet weak var ic_E: UIImageView!
    @IBOutlet weak var ic_F: UIImageView!
    @IBOutlet weak var ic_G: UIImageView!
    @IBOutlet weak var ic_H: UIImageView!
    @IBOutlet weak var ic_I: UIImageView!
    @IBOutlet weak var ic_J: UIImageView!
    @IBOutlet weak var ic_K: UIImageView!
    @IBOutlet weak var ic_L: UIImageView!
    @IBOutlet weak var ic_M: UIImageView!
    @IBOutlet weak var ic_N: UIImageView!
    @IBOutlet weak var ic_O: UIImageView!
    @IBOutlet weak var ic_P: UIImageView!
    @IBOutlet weak var ic_R: UIImageView!
    @IBOutlet weak var ic_S: UIImageView!
    @IBOutlet weak var ic_T: UIImageView!
    @IBOutlet weak var ic_U: UIImageView!
    @IBOutlet weak var ic_V: UIImageView!
    @IBOutlet weak var ic_X: UIImageView!
    @IBOutlet weak var ic_Y: UIImageView!
    @IBOutlet weak var ic_Z: UIImageView!
    @IBOutlet weak var ic_comma: UIImageView!
    @IBOutlet weak var ic_dot: UIImageView!
    @IBOutlet weak var icBack: UIImageView!
    @IBOutlet weak var icSpace: UIImageView!
    
    public var hebrewResponse: ((HebrewWords,String?)->())?
    func addGesture() {
        ic_A.tag = 1
        ic_B.tag = 2
        ic_C.tag = 3
        ic_D.tag = 4
        ic_E.tag = 5
        ic_F.tag = 6
        ic_G.tag = 7
        ic_H.tag = 8
        ic_I.tag = 9
        ic_J.tag = 10
        ic_K.tag = 11
        ic_L.tag = 12
        ic_M.tag = 13
        ic_N.tag = 14
        ic_O.tag = 15
        ic_P.tag = 16
        ic_R.tag = 17
        ic_S.tag = 18
        ic_T.tag = 19
        ic_U.tag = 20
        ic_V.tag = 21
        ic_X.tag = 22
        ic_Y.tag = 23
        ic_Z.tag = 24
        ic_comma.tag = 25
        ic_dot.tag = 26
        icSpace.tag = 27
        icBack.tag = 28
        ic_A.addGestureRecognizer(getGesture())
        ic_B.addGestureRecognizer(getGesture())
        ic_C.addGestureRecognizer(getGesture())
        ic_D.addGestureRecognizer(getGesture())
        ic_E.addGestureRecognizer(getGesture())
        ic_F.addGestureRecognizer(getGesture())
        ic_G.addGestureRecognizer(getGesture())
        ic_H.addGestureRecognizer(getGesture())
        ic_I.addGestureRecognizer(getGesture())
        ic_J.addGestureRecognizer(getGesture())
        ic_K.addGestureRecognizer(getGesture())
        ic_L.addGestureRecognizer(getGesture())
        ic_M.addGestureRecognizer(getGesture())
        ic_N.addGestureRecognizer(getGesture())
        ic_O.addGestureRecognizer(getGesture())
        ic_P.addGestureRecognizer(getGesture())
        ic_R.addGestureRecognizer(getGesture())
        ic_S.addGestureRecognizer(getGesture())
        ic_T.addGestureRecognizer(getGesture())
        ic_U.addGestureRecognizer(getGesture())
        ic_V.addGestureRecognizer(getGesture())
        ic_X.addGestureRecognizer(getGesture())
        ic_Y.addGestureRecognizer(getGesture())
        ic_Z.addGestureRecognizer(getGesture())
        ic_comma.addGestureRecognizer(getGesture())
        ic_dot.addGestureRecognizer(getGesture())
        icSpace.addGestureRecognizer(getGesture())
        icBack.addGestureRecognizer(getGesture())
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc private func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag
        {
        case 1:
            hebrewResponse?(.ic_A,"ש")
            break
        case 2:
            hebrewResponse?(.ic_B,"נ")
            break
        case 3:
            hebrewResponse?(.ic_C,"ב")
            break
        case 4:
            hebrewResponse?(.ic_D,"ג")
            break
        case 5:
            hebrewResponse?(.ic_E,"ק")
            break
        case 6:
            hebrewResponse?(.ic_F,"כ")
            break
        case 7:
            hebrewResponse?(.ic_G,"ע")
            break
        case 8:
            hebrewResponse?(.ic_H,"י")
            break
        case 9:
            hebrewResponse?(.ic_I,"ן")
            break
        case 10:
            hebrewResponse?(.ic_J,"ח")
            break
        case 11:
            hebrewResponse?(.ic_K,"ל")
            break
        case 12:
            hebrewResponse?(.ic_L,"ך")
            break
        case 13:
            hebrewResponse?(.ic_M,"צ")
            break
        case 14:
            hebrewResponse?(.ic_N,"מ")
            break
        case 15:
            hebrewResponse?(.ic_O,"ם")
            break
        case 16:
            hebrewResponse?(.ic_P,"פ")
            break
        case 17:
            hebrewResponse?(.ic_R,"ר")
            break
        case 18:
            hebrewResponse?(.ic_S,"ד")
            break
        case 19:
            hebrewResponse?(.ic_T,"א")
            break
        case 20:
            hebrewResponse?(.ic_U,"ו")
            break
        case 21:
            hebrewResponse?(.ic_V,"ה")
            break
        case 22:
            hebrewResponse?(.ic_X,"ס")
            break
        case 23:
            hebrewResponse?(.ic_Y,"ט")
            break
        case 24:
            hebrewResponse?(.ic_Z,"ז")
            break
        case 25:
            hebrewResponse?(.ic_comma,"ת")
            break
        case 26:
            hebrewResponse?(.ic_dot,"ץ")
            break
        case 27:
            hebrewResponse?(.icSpace," ")
            break
        case 28:
            hebrewResponse?(.icBack,nil)
            break
        default:
            break
        }
    }
    
}
enum HebrewWords: String {
    
    case ic_A = "ic_a_iw"
    case ic_B = "ic_b_iw"
    case ic_C = "ic_c_iw"
    case ic_D = "ic_d_iw"
    case ic_E = "ic_e_iw"
    case ic_F = "ic_f_iw"
    case ic_G = "ic_g_iw"
    case ic_H = "ic_h_iw"
    case ic_I = "ic_i_iw"
    case ic_J = "ic_j_iw"
    case ic_K = "ic_k_iw"
    case ic_L = "ic_l_iw"
    case ic_M = "ic_m_iw"
    case ic_N = "ic_n_iw"
    case ic_O = "ic_o_iw"
    case ic_P = "ic_p_iw"
    case ic_R = "ic_r_iw"
    case ic_S = "ic_s_iw"
    case ic_T = "ic_t_iw"
    case ic_U = "ic_u_iw"
    case ic_V = "ic_v_iw"
    case ic_X = "ic_x_iw"
    case ic_Y = "ic_y_iw"
    case ic_Z = "ic_z_iw"
    case ic_comma = "ic_comma_iw"
    case ic_dot = "ic_dot_iw"
    case icSpace = "space"
    case icBack = "back"
}
class HebrewKeys {
    func getIcon(word: String) -> HebrewWords {
        switch word {
        case "ש":
            return .ic_A
        case "נ":
            return .ic_B
        case "ב":
            return .ic_C
        case "ג":
            return .ic_D
        case "ק":
            return .ic_E
        case "כ":
            return .ic_F
        case "ע":
            return .ic_G
        case "י":
            return .ic_H
        case "ן":
            return .ic_I
        case "ח":
            return .ic_J
        case "ל":
            return .ic_K
        case "ך":
            return .ic_L
        case "צ":
            return .ic_M
        case "מ":
            return .ic_N
        case "ם":
            return .ic_O
        case "פ":
            return .ic_P
        case "ר":
            return .ic_R
        case "ד":
            return .ic_S
        case "א":
            return .ic_T
        case "ו":
            return .ic_U
        case "ה":
            return .ic_V
        case "ס":
            return .ic_X
        case "ט":
            return .ic_Y
        case "ז":
            return .ic_Z
        case "ת":
            return .ic_comma
        case "ץ":
            return .ic_dot
        default:
            return .icSpace
        }
    }
}
