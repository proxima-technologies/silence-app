//
//  ProgressHud.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import Foundation
import UIKit
import JGProgressHUD
class ProgressHud {
    private let hud = JGProgressHUD()
    init(text: String) {
        hud.textLabel.text = text
    }
    func showHud(view: UIView) {
        hud.show(in: view)
    }
    func hideHud() {
        hud.dismiss(animated: true)
    }
    deinit {
        print("Asad")
    }
}
