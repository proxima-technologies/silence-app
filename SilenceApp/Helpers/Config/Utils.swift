//
//  Utils.swift
//  SilenceApp
//
//  Created by Rana Asad on 17/05/2022.
//

import UIKit
class Utils {
    private static var shared: Utils?
    public static func getShared() -> Utils {
        if shared == nil {
            shared = Utils()
        }
        return shared!
    }
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    func getMillis() -> Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    func getNoOfHours(time: Int64) -> String {
        let diffInMillisec = getMillis() - (time)
        var diffInSec = diffInMillisec / 1000
        _ = diffInSec % 60
        diffInSec = diffInSec / 60
        _ = diffInSec % 60
        diffInSec = diffInSec / 60
        let hours = diffInSec % 24
        if hours > 24 {
            let days = (hours / 24)
            let st = String(days) + "Days Ago"
            return st
        } else {
            let st = String(hours) + " Hours Ago"
            return st
        }
    }
    func showAlertWrapper(viewController: UIViewController,alertTitle: String, alertMessage: String) {
            let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert);

            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil);
            alertController.addAction(okAction);

        if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }

                // viewController should now be your topmost view controller
                viewController.present(alertController, animated: true, completion: nil)
            }
        }
}
