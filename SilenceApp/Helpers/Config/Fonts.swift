//
//  Fonts.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import Foundation
import UIKit
enum FontsName: String {
    case PoppinsBold = "Poppins-Bold"
    case PoppinsLight = "Poppins-Light"
    case PoppinsMedium = "Poppins-Medium"
    case PoppinsRegular = "Poppins-Regular"
    case PoppinsSemiBold = "Poppins-SemiBold"
}
enum Fonts {
    case ButtonTitle
}
extension Fonts {
    var font: UIFont {
        get {
            switch self {
            case .ButtonTitle:
                return UIFont(name: FontsName.PoppinsBold.rawValue, size: 15)!
            }
        }
    }
}

