//
//  ConfigManager.swift
//  SilenceApp
//
//  Created by Rana Asad on 20/05/2022.
//

import Foundation
public class ConfigManager{
    
    private var preferences:UserDefaults;
    private static var instance = ConfigManager();
    
    init() {
        preferences=UserDefaults.standard;
    }
    
    public static func getInstance()->ConfigManager{
        return instance
    }
    public func setLanguage(language: String){
        preferences.set(language, forKey: ConfigKeys.LANGUAGE)
        preferences.synchronize()
    }
    public func getLanguage()->String{
        let language = preferences.object(forKey: ConfigKeys.LANGUAGE)
        if let language = language as? String {
            return language
        } else {
            return "en"
        }
    }
    private class ConfigKeys{
        static let LANGUAGE="LANGUAGE"
    }
}

