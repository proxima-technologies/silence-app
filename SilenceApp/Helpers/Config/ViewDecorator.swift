//
//  ViewDecorator.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import Foundation
import UIKit
class ViewDecorator {
    private static var shared: ViewDecorator?
    public static func getInstance() -> ViewDecorator{
        if shared == nil {
            shared = ViewDecorator()
        }
        return shared!
    }
    public func addCircleCornerRadius(view: UIView) {
        view.layer.cornerRadius = view.frame.size.height / 2
    }
    public func addCircleCornerRadiusAndBorderColor(view: UIView,borderColor: Colors) {
        view.layer.cornerRadius = view.frame.size.height / 2
        view.layer.borderColor = borderColor.value.cgColor
        view.layer.borderWidth = 2
    }
    public func addCornerRadius(radius: Radius,view: UIView) {
        view.layer.cornerRadius = radius.value
    }
    public func addCornerRadiusAndBorderColor(view: UIView,radius: Radius,borderColor: Colors) {
        view.layer.cornerRadius = radius.value
        view.layer.borderColor = borderColor.value.cgColor
        view.layer.borderWidth = 1
    }
    public func setLeftPaddingPoints(textField: UITextField,amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: textField.frame.size.height))
        textField.leftView = paddingView
        textField.leftViewMode = .always
    }
    public func addButtonFont(button: UIButton, font: Fonts) {
        button.titleLabel?.font = font.font
    }
    
}
