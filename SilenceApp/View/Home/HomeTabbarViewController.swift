//
//  HomeTabbarViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 17/05/2022.
//

import UIKit

class HomeTabbarViewController: UITabBarController {
    private let tabbar=Bundle.main.loadNibNamed("MainBottomBar", owner: self, options: nil)![0] as! MainBottomBar
    private let progressHud = ProgressHud(text: "Sending...")
    private var userImage: UIImage = UIImage()
    private var url = ""
    private var homeTabbarViewModel = HomeTabbarViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDecorator()
        addGesture()
    }
    private func viewDecorator() {
        if #available(iOS 13.0, *) {
            self.tabBar.isHidden=true
            tabbar.frame=CGRect(x: 0, y: self.view.frame.height - 90, width: self.view.frame.width, height: 90)
            self.view.addSubview(tabbar)
        }
        ViewDecorator.getInstance().addCircleCornerRadius(view: tabbar.cameraMiniView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: tabbar.messageMiniView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: tabbar.profileMiniView)
        switchMiniView(type: 1)
        updateUI()
    }
    private func updateUI() {
        homeTabbarViewModel.imageUploadResponse = {[weak self](url) in
            guard let url = url else {
                return
            }
            self?.progressHud.hideHud()
            self?.sendImageToContact(url: url)
        }
    }
    private func sendImageToContact(url: String) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: MyContactsViewController.self)
        vc.modalPresentationStyle = .fullScreen
        vc.configure(imageUrl: url)
        self.present(vc, animated: true, completion: nil)
    }
    private func switchMiniView(type: Int) {
        switch type {
        case 1:
            tabbar.messageMiniView.isHidden = false
            tabbar.cameraMiniView.isHidden = true
            tabbar.profileMiniView.isHidden = true
            break
        case 2:
            tabbar.messageMiniView.isHidden = true
            tabbar.cameraMiniView.isHidden = false
            tabbar.profileMiniView.isHidden = true
            break
        case 3:
            tabbar.messageMiniView.isHidden = true
            tabbar.cameraMiniView.isHidden = true
            tabbar.profileMiniView.isHidden = false
            break
        default:
            break
        }
    }
    /**
        Function for adding gestures to Views
     */
    private func addGesture() {
        tabbar.messageView.isUserInteractionEnabled = true
        tabbar.messageView.tag = 1
        tabbar.messageView.addGestureRecognizer(getGesture())
        tabbar.cameraView.isUserInteractionEnabled = true
        tabbar.cameraView.tag = 2
        tabbar.cameraView.addGestureRecognizer(getGesture())
        tabbar.profileView.isUserInteractionEnabled = true
        tabbar.profileView.tag = 3
        tabbar.profileView.addGestureRecognizer(getGesture())
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    private func imageUpload(){
        ImagePickerHandler.shared.showActionSheet(vc: self, view: self.view)
        ImagePickerHandler.shared.imagePickedBlock = {[weak self] (img, data) in
            self?.userImage = img
            guard let view = self?.view else {
                return
            }
            self?.progressHud.showHud(view: view)
            self?.homeTabbarViewModel.uploadImage(image: img)
        }
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            self.selectedIndex = 0
            switchMiniView(type: 1)
            break
        case 2:
            imageUpload()
            break
        case 3:
            self.selectedIndex = 1
            switchMiniView(type: 3)
            break
        default:
            break
        }
    }

}
