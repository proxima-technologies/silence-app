//
//  KeyboardMenuViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 20/05/2022.
//

import UIKit

class KeyboardMenuViewController: UIViewController {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var langaugeSegment: UISegmentedControl!
    @IBOutlet weak var doneButton: UIButton!
    
    public var segmentClicked:((Int)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDecorator()
        setLanguageSegment()
    }
    private func setLanguageSegment() {
        let language = ConfigManager.getInstance().getLanguage()
        switch language {
        case "en":
            langaugeSegment.selectedSegmentIndex = 0
            break
        case "ar":
            langaugeSegment.selectedSegmentIndex = 1
            
            break
        case "iw":
            langaugeSegment.selectedSegmentIndex = 2
            break
        default:
            break
        }
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: cardView, radius: .CardRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadius(radius: .ButtonRadius, view: doneButton)
        ViewDecorator.getInstance().addButtonFont(button: doneButton, font: .ButtonTitle)
    }
    @IBAction func doneButtonClicked(_ sender: Any) {
        let index = langaugeSegment.selectedSegmentIndex
        segmentClicked?(index)
        self.dismiss(animated: true)
    }
}
