//
//  MyContactsViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 19/05/2022.
//

import UIKit
import Contacts
import ContactsUI

class MyContactsViewController: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuImageView: UIImageView!
    
    private let contactViewModel = ContactViewModel()
    private var imageUrl: String?
    public var contactsResponse: ((User)->())?
    private let userViewModel = UserDetailViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        userViewModel.getBlockList()
        viewDecorator()
        addGesture()
        updateUI()
    }
    public func configure(imageUrl: String) {
        self.imageUrl = imageUrl
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCircleCornerRadius(view: backView)
        
    }
    private func updateUI() {
        contactViewModel.contactsResponse = {[weak self] in
            self?.tableView.reloadData()
        }
    }
    private func addGesture() {
        searchBar.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        backView.isUserInteractionEnabled = true
        backView.tag = 1
        backView.addGestureRecognizer(getGesture())
        menuImageView.isUserInteractionEnabled = true
        menuImageView.tag = 2
        menuImageView.addGestureRecognizer(getGesture())
    }
    private func addContact() {
        let openContact = CNContact()
        let vc = CNContactViewController(forNewContact: openContact)
        self.present(UINavigationController(rootViewController: vc), animated:true)
    }
    private func showMenu() {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: ContactMenuViewController.self)
        vc.modalPresentationStyle = .fullScreen
        vc.refreshClicked = {[weak self] in
            self?.contactViewModel.getAllUsers()
        }
        vc.contactClicked = {[weak self] in
            self?.addContact()
        }
        vc.groupClicked = {[weak self] in
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            self.dismiss(animated: true)
            break
        case 2:
            showMenu()
            break
        default:
            break
        }
    }
}
extension MyContactsViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactViewModel.contactsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return contactViewModel.configureCell(tableView: tableView, indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentUser = contactViewModel.contactsArray[indexPath.row]
        if StaticData.blockListArr.contains(where: {$0.blockedTo == contactViewModel.contactsArray[indexPath.row].id}) {
            let vc = AppStoryboard.Main.viewController(viewControllerClass: UserDetailViewController.self)
            vc.user = contactViewModel.contactsArray[indexPath.row]
            self.present(vc, animated: true)
        } else {
            contactViewModel.findDocument(receiverUser: currentUser)
            contactViewModel.channelIdResponse = {[weak self](id) in
                let vc = AppStoryboard.Main.viewController(viewControllerClass: ConversationViewController.self)
                vc.configure(receiverUser: currentUser,channelId: id,inbox: nil, imageUrl: self?.imageUrl)
                vc.modalPresentationStyle = .fullScreen
                self?.present(vc, animated: true, completion: nil)
                tableView.deselectRow(at: indexPath, animated: false)
            }
        }
        
    }
}
extension MyContactsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        contactViewModel.contactsArray.removeAll()
        if searchText.isEmpty {
            contactViewModel.contactsArray.append(contentsOf: contactViewModel.tempContacts)
        } else {
            for contact in contactViewModel.tempContacts {
                if contact.name.lowercased().contains(searchText.lowercased()) {
                    contactViewModel.contactsArray.append(contact)
                }
            }
        }
        contactViewModel.contactsResponse?()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}
