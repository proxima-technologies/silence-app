//
//  MessageViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 17/05/2022.
//

import UIKit

class MessageViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newMessageView: UIView!
    
    private let messageViewModel = MessageViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDecorator()
        getMessages()
        updateUI()
        addGesture()
        setDelegates()
        // Do any additional setup after loading the view.
    }
    private func setDelegates() {
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
    }
    private func updateUI() {
        messageViewModel.reloadTableView = {[weak self] in
            self?.tableView.reloadData()
        }
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCircleCornerRadius(view: newMessageView)
    }
    private func getMessages() {
        messageViewModel.getMessages()
    }
    private func addGesture() {
        newMessageView.isUserInteractionEnabled = true
        newMessageView.tag = 1
        newMessageView.addGestureRecognizer(getGesture())
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            let vc = AppStoryboard.Main.viewController(viewControllerClass: MyContactsViewController.self)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            break
        default:
            break
        }
    }
}
extension MessageViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageViewModel.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return messageViewModel.configureCell(tableView: tableView, indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentInbox = messageViewModel.messages[indexPath.row]
        let userId = messageViewModel.getUserId(inbox: currentInbox)
        messageViewModel.getUserDetail(id: userId)
        messageViewModel.channelIdResponse = {[weak self] (id) in
            guard let currentUser = self?.messageViewModel.currentUser else {
                return
            }
            let vc = AppStoryboard.Main.viewController(viewControllerClass:  ConversationViewController.self)
            vc.modalPresentationStyle = .fullScreen
            vc.configure(receiverUser: currentUser, channelId: id, inbox: currentInbox, imageUrl: nil)
            self?.present(vc, animated: true, completion: nil)
            self?.tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
}
extension MessageViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        messageViewModel.messages.removeAll()
        if searchText.isEmpty {
            messageViewModel.messages.append(contentsOf: messageViewModel.tempMessages)
        } else {
            for inbox in messageViewModel.tempMessages {
                if inbox.message.lowercased().contains(searchText.lowercased()) {
                    messageViewModel.messages.append(inbox)
                }
            }
        }
        messageViewModel.reloadTableView?()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}
