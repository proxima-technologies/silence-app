//
//  ConversationViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 23/05/2022.
//

import UIKit
import SDWebImage
import FirebaseAuth
import SwiftUI
class ConversationViewController: UIViewController {
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var langaugeSwitch: UISwitch!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sendIconView: UIView!
    @IBOutlet weak var bottomConstriant: NSLayoutConstraint!
    @IBOutlet weak var typingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var galleryImageView: UIImageView!
    private let englishKeyboard = Bundle.main.loadNibNamed("EnglishKeyboard", owner: ConversationViewController.self, options: nil)![0] as! EnglishKeyboard
    private let arabicKeyboard = Bundle.main.loadNibNamed("ArabicKeyboard", owner: ConversationViewController.self, options: nil)![0] as! ArabicKeyboard
    private let hebrewKeyboard = Bundle.main.loadNibNamed("HebrewKeyboard", owner: ConversationViewController.self, options: nil)![0] as! HebrewKeyboard
    private let conversationViewModel = ConversationViewModel()
    private var isTyping:Bool = false
    private var isTypingTap: Bool = false
    private var receiverUser: User?
    private var channelId: String?
    private var inbox: Inbox?
    private var itemSize = 0
    private var imageUrl: String?
    private let progressHud = ProgressHud(text: "Uploading...")
    private let userViewModel = UserDetailViewModel()
    private var keyboardHeight:CGFloat?
    override func viewDidLoad() {
        super.viewDidLoad()
        conversationViewModel.channelId = self.channelId!
        viewDecorator()
        setupCollectionView()
        setTypingViewHeight()
        addGesture()
        setUpToolbar()
        userViewModel.getBlockList()
        updateUI()
        conversationViewModel.getMessages()
        if let imageUrl = imageUrl {
            sendMessage(messageType: .Image)
        }
        showKeyboard()
        textview.delegate = self
        
    }
    public func configure(receiverUser: User,channelId: String,inbox: Inbox?,imageUrl: String?) {
        self.receiverUser = receiverUser
        self.channelId = channelId
        self.inbox = inbox
        self.imageUrl = imageUrl
    }
    private func calculateHeight(){
        let oldHeight = textview.frame.size.height
        let maxHeight: CGFloat = 150.0 //beyond this value the textView will scroll
        var newHeight = min(textview.sizeThatFits(CGSize(width: textview.frame.width, height: CGFloat.greatestFiniteMagnitude)).height, maxHeight)
        newHeight = max(newHeight, 25)
        textview.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        newHeight = ceil(newHeight)
        enableTextViewScroll(height: newHeight)
        if newHeight != oldHeight {
            UIView.animate(withDuration: 0.2, animations: {
                self.updateTextViewHeight(height: newHeight)
                self.moveToBottom()
            }, completion: { (finished: Bool) in
                
            })
        }
    }
    
    private func updateTextViewHeight(height:CGFloat){
        self.typingViewHeight.constant = height + 50
        self.view.invalidateIntrinsicContentSize()
        self.view.layoutIfNeeded()
    }
    
    private func resetTextViewHeight(){
        textview.text = ""
        typeLabel.isHidden = false
        self.typingViewHeight.constant = 75
        self.view.layoutIfNeeded()
    }
    
    private func enableTextViewScroll(height:CGFloat){
        if height > 150 {
            self.textview.isScrollEnabled=true
        }else{
            self.textview.isScrollEnabled=false
        }
    }
    private func setUpToolbar() {
        guard let receiverUser = receiverUser else {
            return
        }
        userNameLabel.text = receiverUser.name
        statusLabel.text = receiverUser.status
        guard let imageUrl = URL(string: receiverUser.imageUrl) else {
            return
        }
        userImageView.sd_setImage(with: imageUrl)
    }
    private func updateUI() {
        
        tableView.delegate = self
        tableView.dataSource = self
        conversationViewModel.messageResponse = {[weak self](index) in
            if let index = index {
                self?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
            } else {
                self?.tableView.reloadData()
                self?.moveToBottom()
            }
        }
        conversationViewModel.imageUploadResponse = {[weak self](url) in
            if let url = url {
                self?.progressHud.hideHud()
                self?.sendMessage(messageType: .Image)
            }
        }
    }
    private func moveToBottom(){
        if conversationViewModel.messages.count > 0{
            let indexpath=IndexPath(row: conversationViewModel.messages.count-1,section:0)
            self.tableView.scrollToRow(at: indexpath, at: .bottom, animated: true)
        }
    }
    func scrollToBottom()  {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            let point = CGPoint(x: 0 , y: self.tableView.contentSize.height + self.tableView.contentInset.bottom  - self.tableView.frame.height)
            if point.y >= 0{
                self.tableView.setContentOffset(point, animated: true)
            }
        }
        
    }
    private func showKeyboard() {
        switch conversationViewModel.keyboardType {
        case .Hebrew:
            self.textview.isHidden = true
            self.collectionView.isHidden = false
            addHebrewKeyboard()
            break
        case .Arabic:
            self.textview.isHidden = true
            self.collectionView.isHidden = false
            addArabicKeyboard()
            break
        case .English:
            self.textview.isHidden = true
            self.collectionView.isHidden = false
            addEnglishKeyboard()
            break
        case .Text:
            self.textview.isHidden = false
            self.collectionView.isHidden = true
            break
        case .Image:
            break
        }
    }
    private func addArabicKeyboard() {
        arabicKeyboard.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 150)
        self.view.addSubview(arabicKeyboard)
        arabicKeyboard.addGesture()
        arabicKeyboard.ArabicResponse = {[weak self] (input,value) in
            if input == .Z {
                guard let lastIndex = self?.conversationViewModel.signMessages.count else {
                    return
                }
                if lastIndex == 0 {
                    return
                }
                self?.conversationViewModel.signMessages.remove(at: lastIndex - 1)
            } else if input == .space {
                self?.conversationViewModel.addSignMessage(word: input.rawValue, value: value)
            } else {
                self?.conversationViewModel.addSignMessage(word: input.rawValue, value: value)
            }
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
                self?.setTypingViewHeight()
            }
        }
    }
    private func addHebrewKeyboard() {
        hebrewKeyboard.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 150)
        self.view.addSubview(hebrewKeyboard)
        hebrewKeyboard.addGesture()
        hebrewKeyboard.hebrewResponse = {[weak self] (input,value) in
            if input == .icBack {
                guard let lastIndex = self?.conversationViewModel.signMessages.count else {
                    return
                }
                if lastIndex == 0 {
                    return
                }
                self?.conversationViewModel.signMessages.remove(at: lastIndex - 1)
            } else if input == .icSpace {
                self?.conversationViewModel.addSignMessage(word: input.rawValue, value: value)
            } else {
                self?.conversationViewModel.addSignMessage(word: input.rawValue, value: value)
            }
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
                self?.setTypingViewHeight()
            }
        }
    }
    private func addEnglishKeyboard() {
        englishKeyboard.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 150)
        self.view.addSubview(englishKeyboard)
        englishKeyboard.addGesture()
        englishKeyboard.englishResponse = {[weak self] (input,value) in
            if input == .b {
                guard let lastIndex = self?.conversationViewModel.signMessages.count else {
                    return
                }
                if lastIndex == 0 {
                    return
                }
                self?.conversationViewModel.signMessages.remove(at: lastIndex - 1)
            } else if input == .s {
                self?.conversationViewModel.addSignMessage(word: input.rawValue, value: value)
            } else {
                self?.conversationViewModel.addSignMessage(word: input.rawValue, value: value)
            }
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
                self?.setTypingViewHeight()
            }
        }
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: sendView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: sendIconView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: userImageView)
    }
    private func setupCollectionView() {
        let snCollectionViewLayout = SNCollectionViewLayout()
        snCollectionViewLayout.fixedDivisionCount = 10
        snCollectionViewLayout.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = snCollectionViewLayout
    }
    private func setTypingViewHeight() {
        let height = (((conversationViewModel.signMessages.count / 10) + 1)) * (Int(collectionView.frame.width) / 10)
        if itemSize == 0 {
            itemSize = height
        }
        if isTyping {
            typingViewHeight.constant = CGFloat((height) + 80) - CGFloat(itemSize)
        } else {
            typingViewHeight.constant = 70
        }
        self.view.layoutIfNeeded()
    }
    private func setBottomConstraint() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            UIView.animate(withDuration: 0.5, animations: {[weak self] in
                guard let isTypingTap = self?.isTypingTap else {
                    return
                }
                guard let parentViewHeight = self?.view.frame.height else {
                    return
                }
                if isTypingTap {
                    self?.typeLabel.isHidden = true
                    self?.bottomConstriant.constant = 180
                    self?.keyboardContraints(value: parentViewHeight - 180)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        self?.moveToBottom()
                    })
                } else {
                    guard let userTypedCount = self?.conversationViewModel.signMessages.count else {
                        return
                    }
                    if userTypedCount == 0 {
                        self?.typeLabel.isHidden = false
                    } else {
                        self?.typeLabel.isHidden = true
                    }
                    self?.keyboardContraints(value: parentViewHeight)
                    self?.bottomConstriant.constant = 0
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        self?.moveToBottom()
                    })
                }
                self?.view.layoutIfNeeded()
            })
        }
    }
    private func keyboardContraints(value: CGFloat) {
        switch conversationViewModel.keyboardType {
        case .Hebrew:
            hebrewKeyboard.frame.origin.y = value
            break
        case .Arabic:
            arabicKeyboard.frame.origin.y = value
            break
        case .English:
            englishKeyboard.frame.origin.y = value
            break
        case .Text:
            break
        case .Image:
            break
        }
    }
    private func addGesture() {
        sendView.isUserInteractionEnabled = true
        sendView.tag = 1
        sendView.addGestureRecognizer(getGesture())
        sendIconView.isUserInteractionEnabled = true
        sendIconView.tag = 2
        sendIconView.addGestureRecognizer(getGesture())
        userImageView.isUserInteractionEnabled = true
        userImageView.tag = 3
        userImageView.addGestureRecognizer(getGesture())
        userNameLabel.isUserInteractionEnabled = true
        userNameLabel.tag = 3
        userNameLabel.addGestureRecognizer(getGesture())
        backImage.isUserInteractionEnabled = true
        backImage.tag = 4
        backImage.addGestureRecognizer(getGesture())
        galleryImageView.isUserInteractionEnabled = true
        galleryImageView.tag = 5
        galleryImageView.addGestureRecognizer(getGesture())
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            //isHide=false
            bottomConstriant.constant=0
            keyboardHeight = keyboardSize.height
            if #available(iOS 11.0, *) {
                bottomConstriant.constant = self.keyboardHeight!
            } else {
                bottomConstriant.constant = self.keyboardHeight!
            }
            
            self.view.layoutIfNeeded()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                UIView.animate(withDuration: notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval, animations: {
                    self.moveToBottom()
                })
            }
            
        }
    }
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            //isHide=true
            keyboardHeight = keyboardSize.height
            bottomConstriant.constant=0
            if #available(iOS 11.0, *) {
                bottomConstriant.constant=0
            }else{
                bottomConstriant.constant=0
            }
        }
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    private func sendMessage(messageType: MessageType) {
        var text = ""
        switch messageType {
        case .textMessage:
            guard let t = textview.text else {
                return
            }
            text = t
            textview.text = ""
            resetTextViewHeight()
            break
        case .ArabicSign,.HebrewSign,.englishSign:
            text = conversationViewModel.getMessage()
            break
        case .Image:
            text = imageUrl!
            imageUrl = nil
            break
        }
        if text.isEmpty {
            return
        }
        setupInbox(text: text, messageType: messageType)

    }
    private func setupMessage(text: String,messageType: MessageType) {
        guard let inboxId = inbox?.id else {
            return
        }
        conversationViewModel.sendMessage(text: text, inboxId: inboxId, messageType: messageType)
        conversationViewModel.signMessages.removeAll()
        collectionView.reloadData()
    }
    private func setupInbox(text: String,messageType: MessageType) {
        if let inbox = inbox {
            self.setInbox(messageType: messageType, text: text)
            self.setupMessage(text: text, messageType: messageType)
        } else {
            guard let id = receiverUser?.id else {
                return
            }
            conversationViewModel.inboxExist(id: id)
            conversationViewModel.inboxResponse = {[weak self] inbox in
                if let inbox = inbox {
                    self?.inbox = inbox
                    self?.setInbox(messageType: messageType, text: text)
                } else {
                    guard let receiverUser = self?.receiverUser else {
                        return
                    }
                    guard let mineUser = StaticData.user else {
                        return
                    }
                    let id = Utils.getShared().randomString(length: 10)
                    let inbox = Inbox(id: id, inboxType: .userInbox, isDeleted: false, isRead: false, joinedList: [mineUser.id,receiverUser.id], message: text, messageType: messageType, receiverId: receiverUser.id, receiverImage: receiverUser.imageUrl, receiverName: receiverUser.name, senderId: mineUser.id, senderName: mineUser.name, senderImage: mineUser.imageUrl, time: Utils.getShared().getMillis())
                    self?.conversationViewModel.setInbox(inbox: inbox)
                }
                self?.setupMessage(text: text, messageType: messageType)
            }
        }
    }
    private func setInbox(messageType: MessageType,text: String) {
        guard var inbox = inbox else {
            return
        }
        let message = text
        inbox.isRead = false
        inbox.isDeleted = false
        inbox.message = message
        inbox.messageType = messageType
        inbox.time = Utils.getShared().getMillis()
        conversationViewModel.setInbox(inbox: inbox)
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            if langaugeSwitch.isOn {
                isTyping = !isTyping
                isTypingTap = !isTypingTap
                setBottomConstraint()
            } else {
            }
            break
        case 2:
            if StaticData.blockListArr.contains(where: {$0.blockedTo == self.receiverUser!.id}) {
                Utils.getShared().showAlertWrapper(viewController: self, alertTitle: "Error", alertMessage: "Please unblock this user to send message!")
            } else {
                sendMessage(messageType: conversationViewModel.getMessageType())
            }
            break
        case 3:
            let vc = AppStoryboard.Main.viewController(viewControllerClass: UserDetailViewController.self)
            vc.user = self.receiverUser
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
            break
        case 4:
            self.dismiss(animated: true)
            break
        case 5:
            imageUpload()
            break
        default:
            break
        }
    }
    @IBAction func languageSwitchChanged(_ sender: Any) {
        if langaugeSwitch.isOn {
            conversationViewModel.keyboardType = conversationViewModel.tempKeyboardType
            showKeyboard()
        } else {
            conversationViewModel.keyboardType = .Text
            showKeyboard()
        }
    }
    private func imageUpload(){
        ImagePickerHandler.shared.showActionSheet(vc: self, view: self.view)
        ImagePickerHandler.shared.imagePickedBlock = {[weak self] (img, data) in
            guard let view = self?.view else {
                return
            }
            self?.progressHud.showHud(view: view)
            self?.conversationViewModel.uploadImage(image: img)
        }
    }
    
}
extension ConversationViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SNCollectionViewLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return conversationViewModel.signMessages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return conversationViewModel.configureCell(collectionView: collectionView, indexPath: indexPath)
    }
    func scaleForItem(inCollectionView collectionView: UICollectionView, withLayout layout: UICollectionViewLayout, atIndexPath indexPath: IndexPath) -> UInt {
        return 1
    }
}
extension ConversationViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversationViewModel.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = conversationViewModel.configureCell(tableView: tableView, indexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if conversationViewModel.messages[indexPath.row].messageType == .textMessage {
            guard let message = conversationViewModel.messages[indexPath.row].message else {
                return 0
            }
            textLabel.isHidden = true
            textLabel.text = message
            let maximumLabelSize: CGSize = CGSize(width: 276, height: 9999)
            let expectedLabelSize: CGSize = textLabel.sizeThatFits(maximumLabelSize)
            return expectedLabelSize.height + 38
        } else {
            return conversationViewModel.getHeight(index: indexPath.row)
        }
    }
    
}
extension Date {
    
    func toString(withFormat format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let myString = formatter.string(from: self)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = format
        
        return formatter.string(from: yourDate!)
    }
}
//extension ConversationViewController : ContextMenuDelegate {
//    func contextMenuDidSelect(_ contextMenu: ContextMenu, cell: ContextMenuCell, targetedView: UIView, didSelect item: ContextMenuItem, forRowAt index: Int) -> Bool {
//        if index == 0
//        {
//
//        }
//        return true
//    }
//
//    func contextMenuDidDeselect(_ contextMenu: ContextMenu, cell: ContextMenuCell, targetedView: UIView, didSelect item: ContextMenuItem, forRowAt index: Int) {
//
//    }
//
//}
extension ConversationViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text.count>0{
            typeLabel.isHidden=true
        }
        return true
    }
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.count == 0{
            typeLabel.isHidden=false
            
        }else if textView.text.count == 1{
            typeLabel.isHidden=true
            
        }
        calculateHeight()
        
        
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        print("return")
        return true
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        print("hamza")
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textview.text.isEmpty{
            self.view.invalidateIntrinsicContentSize()
            textview.text=""
            textview.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else{
            typeLabel.isHidden=true
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textview.text.isEmpty{
            typeLabel.isHidden=false
        }else{
            typeLabel.isHidden=true
            textview.text=""
            textview.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
}
