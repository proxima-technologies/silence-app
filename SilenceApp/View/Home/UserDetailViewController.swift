//
//  UserDetailViewController.swift
//  SilenceApp
//
//  Created by Tayyab Mubeen on 25/05/2022.
//

import UIKit
import SDWebImage

class UserDetailViewController: UIViewController {

    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var blockButtonOutlet: UIButton!
    var user: User?
    var userViewModel = UserDetailViewModel()
    var isBlock = false
    override func viewDidLoad() {
        super.viewDidLoad()
        userViewModel.getBlockList()
        setupView()
        addGesture()
        viewDecorator()
    }
    func setupView() {
        blockButtonOutlet.layer.cornerRadius  = 10
        if let user = self.user {
            emailTextField.text = user.email
            profileImage.layer.cornerRadius = profileImage.frame.height / 2
            profileImage.sd_setImage(with: URL(string: user.imageUrl))
            statusTextField.text = user.status
            phoneNumberTextField.text = user.phoneNo
            if StaticData.blockListArr.contains(where: {$0.blockedTo == user.id}){
                self.blockButtonOutlet.setTitle("UNBLOCK", for: .normal)
                self.isBlock = true
            } else {
                self.blockButtonOutlet.setTitle("BLOCK", for: .normal)
                self.isBlock = false
            }
        }
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: emailView, radius: .CardRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: statusView, radius: .CardRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: phoneView, radius: .CardRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCircleCornerRadius(view: backView)
    }
    private func addGesture() {
        backView.addGestureRecognizer(getGesture())
        backView.isUserInteractionEnabled = true
        backView.tag = 1
    }
    
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
        self.dismiss(animated: true)
        break
        default:
            break
        }
    }

    @IBAction func blockBtnActn(_ sender: UIButton) {
        if let user = self.user {
            if !self.isBlock{
                userViewModel.blockUser(user: user.id)
                self.isBlock = true
                self.blockButtonOutlet.setTitle("UNBLOCK", for: .normal)
                userViewModel.getBlockList()
                
            } else if self.isBlock {
                userViewModel.unBlockUser(user: user.id)
                self.blockButtonOutlet.setTitle("BLOCK", for: .normal)
                self.isBlock = false
                userViewModel.getBlockList()
                
            }
        }
    }
}

