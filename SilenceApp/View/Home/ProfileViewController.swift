//
//  ProfileViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 19/05/2022.
//

import UIKit
import FirebaseAuth
import SDWebImage
class ProfileViewController: UIViewController {
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var editImageView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailEditImageView: UIImageView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var statusEditImageView: UIImageView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameEditImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var defaultKeyboardButton: UIButton!
    @IBOutlet weak var contactsButton: UIButton!
    
    private let profileViewModel = ProfileViewModel()
    private let progressHud = ProgressHud(text: "Please Wait...")
    private var userImage: UIImage = UIImage()
    private var url = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDecorator()
        updateUI()
        addGesture()
        // Do any additional setup after loading the view.
    }
    private func updateUI() {
        guard let user = StaticData.user else {
            return
        }
        guard let url = URL(string: user.imageUrl) else {
            return
        }
        userImageView.sd_setImage(with: url)
        nameTextField.text = user.name
        emailTextField.text = user.email
        statusTextField.text = user.status
        phoneTextField.text = user.phoneNo
        profileViewModel.imageUploadResponse = {[weak self] (url) in
            guard let url = url else {
                return
            }
            
            self?.userImageView.image = self?.userImage
            guard var user = StaticData.user else {
                return
            }
            user.imageUrl = url
            self?.profileViewModel.updateUser(user: user)
            self?.progressHud.hideHud()
        }
    }
    private func viewDecorator() {
        nameTextField.isUserInteractionEnabled = false
        emailTextField.isUserInteractionEnabled = false
        statusTextField.isUserInteractionEnabled = false
        phoneTextField.isUserInteractionEnabled = false
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: logoutView, radius: .ButtonRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: emailView, radius: .CardRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: statusView, radius: .CardRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: phoneView, radius: .CardRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: defaultKeyboardButton)
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: contactsButton)
        ViewDecorator.getInstance().addCircleCornerRadius(view: editImageView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: userImageView)
        ViewDecorator.getInstance().addButtonFont(button: defaultKeyboardButton, font: .ButtonTitle)
        ViewDecorator.getInstance().addButtonFont(button: contactsButton, font: .ButtonTitle)
    }
    /**
        Function for adding gestures to Views
     */
    private func addGesture() {
        logoutView.isUserInteractionEnabled = true
        logoutView.tag = 1
        logoutView.addGestureRecognizer(getGesture())
        editImageView.isUserInteractionEnabled = true
        editImageView.tag = 2
        editImageView.addGestureRecognizer(getGesture())
        nameEditImageView.isUserInteractionEnabled = true
        nameEditImageView.tag = 3
        nameEditImageView.addGestureRecognizer(getGesture())
        emailEditImageView.isUserInteractionEnabled = true
        emailEditImageView.tag = 4
        emailEditImageView.addGestureRecognizer(getGesture())
        statusEditImageView.isUserInteractionEnabled = true
        statusEditImageView.tag = 5
        statusEditImageView.addGestureRecognizer(getGesture())
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
           view.addGestureRecognizer(tap)
    }
    private func updateUI(profileEdit: ProfileEdit) {
        switch profileEdit {
        case .Name:
            nameEditImageView.image = Icons.checkIcon
            nameTextField.isUserInteractionEnabled = true
            nameTextField.becomeFirstResponder()
            break
        case .Email:
            emailEditImageView.image = Icons.checkIcon
            emailTextField.isUserInteractionEnabled = true
            emailTextField.becomeFirstResponder()
            break
        case .Status:
            statusEditImageView.image = Icons.checkIcon
            statusTextField.isUserInteractionEnabled = true
            statusTextField.becomeFirstResponder()
            break
        case .Unknown:
            nameEditImageView.image = Icons.editIcon
            emailEditImageView.image = Icons.editIcon
            statusEditImageView.image = Icons.editIcon
            nameTextField.isUserInteractionEnabled = false
            emailTextField.isUserInteractionEnabled = false
            statusTextField.isUserInteractionEnabled = false
            guard var user = StaticData.user else {
                return
            }
            guard let name = nameTextField.text else {
                return
            }
            guard let email = emailTextField.text else {
                return
            }
            guard let status = statusTextField.text else {
                return
            }
            user.name = name
            user.email = email
            user.status = status
            profileViewModel.updateUser(user: user)
            StaticData.user = user
            break
        }
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    private func imageUpload(){
        ImagePickerHandler.shared.showActionSheet(vc: self, view: self.view)
        ImagePickerHandler.shared.imagePickedBlock = {[weak self] (img, data) in
            guard let rootView = self?.view else {
                return
            }
            self?.userImage = img
            self?.progressHud.showHud(view: rootView)
            self?.profileViewModel.uploadImage(image: img)
        }
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            do {
                try Auth.auth().signOut()
                let vc = AppStoryboard.Main.viewController(viewControllerClass: SplashViewController.self)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } catch {
                
            }
            break
        case 2:
            imageUpload()
            break
        case 3:
            if profileViewModel.profileEdit == .Name {
                profileViewModel.profileEdit = .Unknown
            } else {
                profileViewModel.profileEdit = .Name
            }
            updateUI(profileEdit: profileViewModel.profileEdit)
            break
        case 4:
            if profileViewModel.profileEdit == .Email {
                profileViewModel.profileEdit = .Unknown
            } else {
                profileViewModel.profileEdit = .Email
            }
            updateUI(profileEdit: profileViewModel.profileEdit)
            break
        case 5:
            if profileViewModel.profileEdit == .Status {
                profileViewModel.profileEdit = .Unknown
            } else {
                profileViewModel.profileEdit = .Status
            }
            updateUI(profileEdit: profileViewModel.profileEdit)
            break
        default:
            break
        }
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @IBAction func contactsButtonClicked(_ sender: Any) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: MyContactsViewController.self)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func defaultButtonClicked(_ sender: Any) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: KeyboardMenuViewController.self)
        vc.modalPresentationStyle = .fullScreen
        vc.segmentClicked = {(index) in
            switch index {
            case 0:
                ConfigManager.getInstance().setLanguage(language: "en")
                break
            case 1:
                ConfigManager.getInstance().setLanguage(language: "ar")
                break
            case 2:
                ConfigManager.getInstance().setLanguage(language: "iw")
                break
            default:
                break
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
}
