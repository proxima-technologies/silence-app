//
//  ContactMenuViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 20/05/2022.
//

import UIKit

class ContactMenuViewController: UIViewController {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var refreshLabel: UILabel!
    @IBOutlet weak var addContactLabel: UILabel!
    @IBOutlet weak var createGroupLabel: UILabel!
    
    public var refreshClicked:(()->())?
    public var contactClicked:(()->())?
    public var groupClicked:(()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDecorator()
        addGesture()
        // Do any additional setup after loading the view.
    }
    private func viewDecorator() {
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: cardView, radius: .CardRadius, borderColor: .borderColor)
    }
    private func addGesture() {
        refreshLabel.isUserInteractionEnabled = true
        refreshLabel.tag = 1
        refreshLabel.addGestureRecognizer(getGesture())
        addContactLabel.isUserInteractionEnabled = true
        addContactLabel.tag = 2
        addContactLabel.addGestureRecognizer(getGesture())
        createGroupLabel.isUserInteractionEnabled = true
        createGroupLabel.tag = 3
        createGroupLabel.addGestureRecognizer(getGesture())
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            refreshClicked?()
            self.dismiss(animated: true)
            break
        case 2:
            self.dismiss(animated: true, completion: {[weak self] in
                self?.contactClicked?()
            })
            break
        case 3:
            groupClicked?()
            self.dismiss(animated: true)
            break
        default:
            break
        }
    }
}
