//
//  AuthViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import UIKit
import CountryPicker
import Toast
class AuthViewController: UIViewController {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var countryCodeView: UIView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var getCodeButton: UIButton!
    
    private let authViewModel = AuthViewModel()
    private let progressHud = ProgressHud(text: "Sending Code...")
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDecorator()
        addGesture()
        updateUI()
        // Do any additional setup after loading the view.
    }
    /**
        Function for Decoration of Views
     */
    private func viewDecorator() {
        ViewDecorator.getInstance().addButtonFont(button: getCodeButton, font: .ButtonTitle)
        ViewDecorator.getInstance().setLeftPaddingPoints(textField: phoneNumberTextField, amount: 10)
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: cardView)
        ViewDecorator.getInstance().addCornerRadius(radius: .ButtonRadius, view: getCodeButton)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: phoneNumberTextField, radius: .TextFieldRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: countryCodeView, radius: .TextFieldRadius, borderColor: .borderColor)
    }
    private func updateUI() {
        authViewModel.codeSent = {[weak self] (verificationId,error) in
            self?.progressHud.hideHud()
            if let id = verificationId {
                self?.moveToOTPView(verificationId: id)
            } else {
                guard let err = error else {
                    return
                }
                self?.showToast(title: err)
            }
        }
    }
    private func showToast(title: String) {
        let toast = Toast.text(title)
        toast.show(haptic: .error,after: 1)
    }
    private func moveToOTPView(verificationId: String) {
        guard let phoneNumber = phoneNumberTextField.text else {
            return
        }
        guard let countryCode = countryCodeLabel.text else {
            return
        }
        let vc = AppStoryboard.Main.viewController(viewControllerClass: OTPViewController.self)
        vc.modalPresentationStyle = .fullScreen
        vc.configure(verificationId: verificationId, phoneNumber: countryCode + phoneNumber)
        self.present(vc, animated: true, completion: nil)
    }
    /**
        Function for adding gestures to Views
     */
    private func addGesture() {
        countryCodeView.isUserInteractionEnabled = true
        countryCodeView.tag = 1
        countryCodeView.addGestureRecognizer(getGesture())
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
           view.addGestureRecognizer(tap)
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    
    private func showCountryPicker() {
        let countryPicker = CountryPickerViewController()
        countryPicker.delegate = self
        self.present(countryPicker, animated: true)
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            showCountryPicker()
            break
        default:
            break
        }
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @IBAction func getCodeClicked(_ sender: Any) {
        if phoneNumberTextField.text!.isEmpty {
          return
        }
        guard let countryCode = countryCodeLabel.text else {
            return
        }
        guard let phoneNumber = phoneNumberTextField.text else {
            return
        }
        progressHud.showHud(view: self.view)
        authViewModel.getCode(countryCode: countryCode, phoneNumber: phoneNumber)
    }
    
}
extension AuthViewController: CountryPickerDelegate {
    func countryPicker(didSelect country: Country) {
        countryCodeLabel.text =  "+" + country.phoneCode
    }
}
