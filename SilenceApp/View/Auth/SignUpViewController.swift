//
//  SignUpViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 17/05/2022.
//

import UIKit
import Toast
class SignUpViewController: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var userImageCardView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNoTextField: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    
    private var phoneNumber: String = ""
    private let progressHud = ProgressHud(text: "Please Wait...")
    private let signUpViewModel = SignUpViewModel()
    private var userImage: UIImage = UIImage()
    private var url = ""
    public func configure(phoneNumber: String) {
        self.phoneNumber = phoneNumber
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDecorator()
        initailizeViews()
        addGesture()
        updateUI()
    }
    
    private func updateUI() {
        signUpViewModel.imageUploadResponse = {[weak self] url in
            self?.progressHud.hideHud()
            guard let url = url else {
                return
            }
            self?.url = url
            self?.userImageView.image = self?.userImage
        }
        signUpViewModel.signUpResponse = {[weak self] (error) in
            self?.progressHud.hideHud()
            if let err = error {
                self?.showToast(title: err)
            } else {
                self?.rootViewController()
            }
        }
    }
    func rootViewController() -> Void {
        if #available(iOS 13.0, *) {
            if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene {
                if let delegate = windowScene.delegate as? SceneDelegate {
                    if let window = delegate.window {
                        let rootVC = AppStoryboard.Main.viewController(viewControllerClass: HomeTabbarViewController.self)
                        window.rootViewController = rootVC
                        window.makeKeyAndVisible()
                    }
                }
            }
        }
    }
    private func showToast(title: String) {
        let toast = Toast.text(title)
        toast.show(haptic: .error,after: 1)
    }
    private func initailizeViews() {
        phoneNoTextField.text = phoneNumber
    }
    /**
     Function for Decoration of Views
     */
    private func viewDecorator() {
        ViewDecorator.getInstance().addCircleCornerRadius(view: userImageView)
        ViewDecorator.getInstance().addButtonFont(button: doneButton, font: .ButtonTitle)
        ViewDecorator.getInstance().addCornerRadius(radius: .ButtonRadius, view: doneButton)
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: cardView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: backView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: userImageCardView)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: aboutTextView, radius: .ButtonRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: fullNameTextField, radius: .ButtonRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: aboutTextView, radius: .ButtonRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: emailTextField, radius: .ButtonRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCornerRadiusAndBorderColor(view: phoneNoTextField, radius: .ButtonRadius, borderColor: .borderColor)
        ViewDecorator.getInstance().addCircleCornerRadiusAndBorderColor(view: userImageCardView, borderColor: .primaryColor)
        ViewDecorator.getInstance().setLeftPaddingPoints(textField: emailTextField, amount: 8)
        ViewDecorator.getInstance().setLeftPaddingPoints(textField: fullNameTextField, amount: 8)
        ViewDecorator.getInstance().setLeftPaddingPoints(textField: phoneNoTextField, amount: 8)
        aboutTextView.contentInset = UIEdgeInsets(top: 2, left: 5, bottom: 0, right: 0)
    }
    private func imageUpload(){
        ImagePickerHandler.shared.showActionSheet(vc: self, view: self.view)
        ImagePickerHandler.shared.imagePickedBlock = { (img, data) in
            self.userImage = img
            self.progressHud.showHud(view: self.view)
            self.signUpViewModel.uploadImage(image: img)
        }
    }
    private func signUpUser() {
        if fullNameTextField.text!.isEmpty || emailTextField.text!.isEmpty || aboutTextView.text!.isEmpty {
            
        } else {
            guard let name = fullNameTextField.text else {
                return
            }
            guard let email = emailTextField.text else {
                return
            }
            guard let about = aboutTextView.text else {
                return
            }
            progressHud.showHud(view: self.view)
            signUpViewModel.signUp(email: email, fullName: name, url: url, about: about, phoneNo: phoneNumber)
        }
    }
    private func addGesture() {
        aboutTextView.delegate = self
        backView.isUserInteractionEnabled = true
        backView.tag = 1
        backView.addGestureRecognizer(getGesture())
        userImageCardView.isUserInteractionEnabled = true
        userImageCardView.tag = 2
        userImageCardView.addGestureRecognizer(getGesture())
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
           view.addGestureRecognizer(tap)
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            self.dismiss(animated: true)
            break
        case 2:
            imageUpload()
            break
        default:
            break
        }
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @IBAction func doneButtonClicked(_ sender: Any) {
        signUpUser()
    }
}
extension SignUpViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text.isEmpty {
            aboutLabel.isHidden = false
        } else {
            aboutLabel.isHidden = true
        }
        return true
    }
}
