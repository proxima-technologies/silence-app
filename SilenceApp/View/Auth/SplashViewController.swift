//
//  ViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 10/05/2022.
//

import UIKit
import FirebaseAuth
class SplashViewController: UIViewController {
    let splashViewModel = SplashViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if let userId = Auth.auth().currentUser?.uid {
            splashViewModel.getUserDetails(userId: userId)
        } else {
            let vc = AppStoryboard.Main.viewController(viewControllerClass:  AuthViewController.self)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    func updateUI() {
        splashViewModel.userResponse = {[weak self] (user,error) in
            StaticData.user = user
            let vc = AppStoryboard.Main.viewController(viewControllerClass:  HomeTabbarViewController.self)
            vc.modalPresentationStyle = .fullScreen
            self?.present(vc, animated: true, completion: nil)
        }
    }

}

