//
//  OTPViewController.swift
//  SilenceApp
//
//  Created by Rana Asad on 13/05/2022.
//

import UIKit
import OTPFieldView
import Toast
class OTPViewController: UIViewController {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var descriptionlabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var otpView: OTPFieldView!
    @IBOutlet weak var resendLabel: UILabel!
    
    private let descriptionText = "Enter passcode we send you on "
    private var phoneNumber: String = ""
    private var otp: String = ""
    private let progressHud = ProgressHud(text: "Verifying...")
    private let otpViewModel = OTPViewModel()
    private let authViewModel = AuthViewModel()
    public func configure(verificationId: String, phoneNumber: String) {
        otpViewModel.verificationId = verificationId
        self.phoneNumber = phoneNumber
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        addGesture()
        initailizeViews()
        updateUI()
        setupOtpView()
        viewDecorator()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    private func updateUI() {
        otpViewModel.response = {[weak self] (response,error) in
            if let err = error {
                self?.showToast(title: err)
            } else {
                guard let res = response else {
                    return
                }
                self?.otpViewModel.getUserDetail(id: res)
            }
            
        }
        otpViewModel.userResponse = {[weak self] (userRes,error) in
            if error != nil {
                guard let phoneNumber = self?.phoneNumber else {
                    return
                }
                let vc = AppStoryboard.Main.viewController(viewControllerClass: SignUpViewController.self)
                vc.configure(phoneNumber: phoneNumber)
                vc.modalPresentationStyle = .fullScreen
                self?.present(vc, animated: true, completion: nil)
            } else {
                guard let user = userRes else {
                    return
                }
                self?.rootViewController()
            }
        }
    }
    func rootViewController() -> Void {
        if #available(iOS 13.0, *) {
            if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene {
                if let delegate = windowScene.delegate as? SceneDelegate {
                    if let window = delegate.window {
                        let rootVC = AppStoryboard.Main.viewController(viewControllerClass: HomeTabbarViewController.self)
                        window.rootViewController = rootVC
                        window.makeKeyAndVisible()
                    }
                }
            }
        }
    }
    private func showToast(title: String) {
        let toast = Toast.text(title)
        toast.show(haptic: .error,after: 1)
    }
    private func initailizeViews() {
        descriptionlabel.text = "\(descriptionText)\(phoneNumber)"
    }
    /**
     Function for Decoration of Views
     */
    private func viewDecorator() {
        ViewDecorator.getInstance().addButtonFont(button: doneButton, font: .ButtonTitle)
        ViewDecorator.getInstance().addCornerRadius(radius: .ButtonRadius, view: doneButton)
        ViewDecorator.getInstance().addCornerRadius(radius: .CardRadius, view: cardView)
        ViewDecorator.getInstance().addCircleCornerRadius(view: backView)
    }
    private func setupOtpView(){
        otpView.fieldsCount = 6
        otpView.fieldBorderWidth = 1
        otpView.defaultBorderColor = Colors.borderColor.value
        otpView.filledBorderColor = Colors.primaryColor.value
        otpView.cursorColor = Colors.primaryColor.value
        otpView.displayType = .roundedCorner
        otpView.fieldSize = 35
        otpView.separatorSpace = 8
        otpView.shouldAllowIntermediateEditing = false
        otpView.delegate = self
        otpView.initializeUI()
    }
    private func addGesture() {
        backView.isUserInteractionEnabled = true
        backView.tag = 1
        backView.addGestureRecognizer(getGesture())
        resendLabel.isUserInteractionEnabled = true
        resendLabel.tag = 2
        resendLabel.addGestureRecognizer(getGesture())
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 1:
            self.dismiss(animated: true)
            break
        case 2:
            otpViewModel.resendCode(phoneNumber: phoneNumber)
            break
        default:
            break
        }
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @IBAction func doneButtonClicked(_ sender: Any) {
        progressHud.showHud(view: self.view)
        otpViewModel.authenticateOTP(otp: self.otp)
    }
    
}
extension OTPViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        otp = otpString
    }
}
